SocializeFriendAddPopup = {}

function SocializeFriendAddPopup.popup(friendsFrame)
    StaticPopupDialogs["SOCIALIZE_ADD_FRIEND"] = {
        text = "Enter friend name", -- this is the prompt text
        button1 = "Add", -- confirm button
        button2 = "Cancel", -- cancel button
        hasEditBox = true, -- shows an edit box for user input
        OnAccept = function(self)
            local friendName = self.editBox:GetText():gsub("^%l", string.upper) -- gets user input ucfirst
            local friend = SocializeStorage.addFriend(friendName)
            local friendFrame = friendsFrame:getFriendFrame(friendName)
            if not friendFrame then
                friendFrame = friendsFrame:addFriend(friend)
            else
                friendFrame:show(friend)
            end
            SocializeCommunicationInternal.sendFriendRequest(friend.name)
            --SocializeFriendsFrame.getInstance():updateComponent()

        end,
        OnShow = function (self, data)
            self.editBox:SetFocus()
        end,
        timeout = 0, -- popup will remain until manually closed
        whileDead = true, -- popup can be shown while dead
        hideOnEscape = true, -- hides pop-up when ESC is pressed
        preferredIndex = 3,  -- avoid taint ui issues by using unique indices
        EditBoxOnEnterPressed = function(self)
            local parent = self:GetParent()
            parent.button1:Click()
        end,
    }

    -- How to show the popup
    StaticPopup_Show ("SOCIALIZE_ADD_FRIEND")
end
