---@class SocializeFriendsFrame
SocializeFriendsFrame = {
    ---@type SocializeFriendFrame[]
    friendsList = {},
    mainFrame = {},
    scrollFrame = {},
    childFrame = {},
    scrollbar = {},
    settingAnchor = {},
    socializeFrame = {},
    selectedFriend = nil,
    ---@type SocializeFriendsFrame
    instance = nil,
    searchValue = "",
    friendsMeta = {},
}
---@return SocializeFriendsFrame
function SocializeFriendsFrame.getInstance()
    return SocializeFriendsFrame.instance
end

---
---@param socializeFrame SocializeFrame
---@return SocializeFriendsFrame
function SocializeFriendsFrame:new(socializeFrame)
    if SocializeFriendsFrame.instance then
        return SocializeFriendsFrame.instance
    end
    ---@type SocializeFriendsFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeFriendsFrame.__index = self
    instance.socializeFrame = socializeFrame
    instance.mainFrame = socializeFrame.mainFrame
    instance:initScrollFrame()
    instance:initChildFrame()
    instance:initScrollbar()
    instance:initFriendAnchor()
    instance:activateScrollbarListener()
    instance:activateScrollFrameListener()
    SocializeFriendsFrame.instance = instance
    return instance
end

function SocializeFriendsFrame:addFriend(friend)
    local friendsListSize = SocializeGlobalHelper.getTableLength(self.friendsList)
    local anchor = {}
    if friendsListSize > 0 then
        anchor = self.friendsList[friendsListSize].mainFrame
    else
        anchor = self.settingAnchor
    end
    local friendFrame = SocializeFriendFrame:new(self, friend, self.childFrame, anchor)
    table.insert(self.friendsList, friendFrame)
end

function SocializeFriendsFrame:initScrollFrame()
    local scrollFrame = CreateFrame("ScrollFrame", nil, self.mainFrame)
    scrollFrame:SetPoint("TOPLEFT", 5, -68)
    scrollFrame:SetPoint("BOTTOMRIGHT", -20, 27)
    scrollFrame:EnableMouseWheel(true)
    self.scrollFrame = scrollFrame
end

function SocializeFriendsFrame:initChildFrame()
    local childFrame = CreateFrame("Frame", nil, self.scrollFrame)
    childFrame:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        tile = true, tileSize = 16, edgeSize = 16,
        insets = { left = 0, right = 0, top = 0, bottom = 0 }
    });
    childFrame:SetBackdropColor(0, 0, 0, 0.5); -- set color, 4th argument is alpha (transparency)
    self.childFrame = childFrame
    self:updateChildFrameSize()
end

function SocializeFriendsFrame:initScrollbar()
    local scrollbar = CreateFrame("Slider", nil, self.scrollFrame, "UIPanelScrollBarTemplate")
    --scrollbar:SetPoint("TOPLEFT", self.mainFrame, "TOPRIGHT", -20, -22-60)
    --scrollbar:SetPoint("BOTTOMLEFT", self.mainFrame, "BOTTOMRIGHT", -20, 27)
    scrollbar:SetPoint("TOPRIGHT", self.mainFrame, "TOPRIGHT", -4, -86)
    scrollbar:SetPoint("BOTTOMRIGHT", self.mainFrame, "BOTTOMRIGHT", -4, 41)
    self.scrollbar = scrollbar
    self:updateScrollBarMax()
end

function SocializeFriendsFrame:initFriendAnchor()
    local friendAnchor = CreateFrame("Frame", "FriendAnchor", self.childFrame)
    friendAnchor:SetPoint("TOPLEFT", self.childFrame, "TOPLEFT", 0, 0)
    friendAnchor:SetSize(1, 1)
    self.settingAnchor = friendAnchor
end

function SocializeFriendsFrame:updateScrollBarMax()
    local scrollbarMax = self:calculateChildFrameHeight() - self.scrollFrame:GetHeight();
    local scrollbarMin = 0
    if scrollbarMax < scrollbarMin then
        scrollbarMax = 0
    end
    self.scrollbar:SetMinMaxValues(scrollbarMin, scrollbarMax)
end

function SocializeFriendsFrame:updateChildFrameSize()
    self.childFrame:SetSize(325, self:calculateChildFrameHeight() - self.scrollFrame:GetHeight())
end

function SocializeFriendsFrame:updateScrollFrameChild()
    self.scrollFrame:SetScrollChild(self.childFrame)
end

function SocializeFriendsFrame:activateScrollbarListener()
    self.scrollbar:SetScript("OnValueChanged", function (scrollbarSelf, value)
        scrollbarSelf:GetParent():SetVerticalScroll(value)
    end)
end

function SocializeFriendsFrame:activateScrollFrameListener()
    self.scrollFrame:SetScript("OnMouseWheel", function(_, delta)
        local currentValue = self.scrollbar:GetValue()
        local changedValue = currentValue - (delta*35)
        self.scrollbar:SetValue(changedValue)
    end)
end

function SocializeFriendsFrame:calculateChildFrameHeight()
    ---@param friendFrame SocializeFriendFrame
    local hiddenFriends = SocializeGlobalHelper.getTableLength(self.friendsList, function(friendFrame)
        return friendFrame.mainFrame:IsVisible()
    end)
    return 35 * hiddenFriends
end

function SocializeFriendsFrame:getFriendFrame(friendName)
    for key, friendFrame in pairs(self.friendsList) do
        if friendFrame.friend.name == friendName then
            return friendFrame
        end
    end
    return nil
end

---@param a SocializeFriendFrame
---@param b SocializeFriendFrame
function SocializeFriendsFrame.sortFriends(a, b)
    local resultA = SocializeFriendsFrame.getFriendFrameSortingValue(a)
    local resultB = SocializeFriendsFrame.getFriendFrameSortingValue(b)
    if resultA == resultB then
        return a:resolveFriendName() < b:resolveFriendName()
    end
    return resultA > resultB
end

---@param friendFrame SocializeFriendFrame
function SocializeFriendsFrame.getFriendFrameSortingValue(friendFrame)
    -- 0 removed
    -- 1 no in search result
    -- 2 ignored
    -- 3 offline
    -- 4 online
    -- 5 the player him/herself
    local result = 3
    if friendFrame.isDeleted then
        result = 0
    elseif not friendFrame.isPartOfSearchResult then
        result = 1
    elseif friendFrame.friend.ignored then
        result = 2
    elseif friendFrame.friend.name == UnitName("player") then
        result = 5
    elseif friendFrame.friend.online then
        result = 4
    end
    return result
end

function SocializeFriendsFrame:spawnFriends(friends)
    for key, friend in pairs(friends) do
        local friendFrame = self:addFriend(friend)
        previousReference = friendFrame
        table.insert(self.friendsList, friendFrame)
    end
end

function SocializeFriendsFrame:updateComponent()
    local filteredFriends = self:filteredFriendFrames(self.friendsList)
    local friendsMeta = {
        total = 0,
    }
    for _, friendFrame in ipairs(self.friendsList) do
        if #filteredFriends and not SocializeGlobalHelper.existsInTable(filteredFriends, friendFrame) then
            friendFrame.mainFrame:Hide()
            friendFrame.isPartOfSearchResult = false
        elseif not friendFrame.isDeleted then
            friendFrame.isPartOfSearchResult = true
            friendFrame.mainFrame:Show()
        end
        local sortingScore = self.getFriendFrameSortingValue(friendFrame)
        if not friendsMeta[sortingScore] then
            friendsMeta[sortingScore] = 0
        end
        friendsMeta[sortingScore] = friendsMeta[sortingScore] + 1
        if sortingScore ~= 0 then --dont count invisible frames
            friendsMeta.total = friendsMeta.total + 1
        end
    end
    self.friendsMeta = friendsMeta -- pass data to top layer
    table.sort(self.friendsList, self.sortFriends) --sort uses visibility state for ordering
    local previousReference = { mainFrame = self.settingAnchor }
    for key, friendFrame in ipairs(self.friendsList) do
        friendFrame:anchorTo(previousReference.mainFrame)
        friendFrame:updateComponent()
        previousReference = friendFrame
    end
    self:updateChildFrameSize()
    self:updateScrollFrameChild()
    self:updateScrollBarMax()
end

---@return SocializeFriendFrame[]
function SocializeFriendsFrame:filteredFriendFrames(friendList)
    ---@param friendFrame SocializeFriendFrame
    return SocializeGlobalHelper.filterTable(friendList, function(friendFrame)
        local foundSearchValueInString = string.find(string.lower(friendFrame:resolveFriendName()), string.lower(self.searchValue))
        local isSelf = SocializeSession.isSelf(friendFrame.friend.name)
        if foundSearchValueInString or isSelf then
            return true
        end
    end)
end

function SocializeFriendsFrame:setSelectedFriend(friend)
    self.selectedFriend = friend
    self.socializeFrame.searchBox:ClearFocus()
end

function SocializeFriendsFrame:getSelectedFriend()
    if self.selectedFriend then
        return SocializeFriendWrapper:new(self.selectedFriend)
    end
    return false
end
