SocializeStatusUpdatePopup = {}

function SocializeStatusUpdatePopup.popup(statusTextElement)
    StaticPopupDialogs["SOCIALIZE_SET_STATUSTEXT"] = {
        text = "Enter status text",
        button1 = "Go",
        button2 = "Back",
        hasEditBox = true,
        OnShow = function (self, data)
            self.editBox:SetText(SocializeStorage.getPlayer().status)
            self.editBox:SetFocus()
        end,
        OnAccept = function(self)
            local status = self.editBox:GetText()
            SocializeStorage.getPlayer().status = status
            statusTextElement:SetText("Status: "..status)
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
        EditBoxOnEnterPressed = function(self)
            local parent = self:GetParent()
            parent.button1:Click()
        end,
    }

    StaticPopup_Show ("SOCIALIZE_SET_STATUSTEXT")
end