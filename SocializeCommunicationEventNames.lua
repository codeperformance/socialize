SocializeCommunicationEventNames = {
    prefix = "SZE_"
}

SocializeCommunicationEventNames.friendRequestEvent = SocializeCommunicationEventNames.prefix .. "ADD_FRIEND"
SocializeCommunicationEventNames.areYouOnlineEvent = SocializeCommunicationEventNames.prefix .. "R_U_ONLINE"
SocializeCommunicationEventNames.iGoOfflineEvent = SocializeCommunicationEventNames.prefix .. "I_GO_OFFLINE"
SocializeCommunicationEventNames.iAmOnlineEvent = SocializeCommunicationEventNames.prefix .. "I_M_ONLINE"
SocializeCommunicationEventNames.iAmReloadingEvent = SocializeCommunicationEventNames.prefix .. "I_M_RL"
SocializeCommunicationEventNames.shareEquipmentEvent = SocializeCommunicationEventNames.prefix .. "S_EQUIPMENT"
SocializeCommunicationEventNames.shareProgressEvent = SocializeCommunicationEventNames.prefix .. "S_PROGRESS"