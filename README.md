# Socialize
Friends list on steroids

#changelog:
1. [Backend] Split project into multiple files for easier future development.
2. [Frontend] Added frame for selected friend with tabs for Gear, Achievements, Enchants, Other.
3. [Frontend] Changed color of friend-frame to reflect original UI.
4. [Frontend] Added Ignore-feature.
5. [Backend] File restructuring. Logic segmentation. 
6. [Frontend] Added feature to share equipment and to show friend's equipment.
7. [backend] If friend is not responding for 30 seconds, stop pinging friend
8. [frontend] Added feature to share progress and to show friend's progress.
9. [frontend] Added mythic keystone level to location information
10. [backend] Added support for auto-adding alts
11. [frontend] Added settings panel
12. [frontend] Added search function
13. [frontend] Added online status dropdown
14. [frontend] Added counter to show online and total friends in list(adjusted by search)
15. [frontend] Added support for manastorm level in location information
16. [frontend] Ensured popups get focus when shown
17. [frontend] Updated ID system and named it "nickname"
18. [frontend] You always see yourself on top of friendlist with a special border. You automatically share progress and gear with yourself
19. [frontend] You don't share specific location when pvping
20. [frontend] Ensured all lvl 70 raids are in the progress overview
21. [frontend] Context menu not does not show unlogical options for specific friends
22. [frontend] If you merged 2 friends under the same nickname by accident, you can now change the nickname of just 1 by unchecking the option "Change name for all related characters/alts"

## Features:
See all your friends, no matter on what character you are playing.

- Add friend, once they accept your request, you can see if they are online.
- When logging into a (new) alt, you can ask your friends to add your alt, so you can again, see their online status.
- Friends now share locations.
- Friends now share custom status text.
- You can now select a friend in the list and see the friends stats.
- Friends can now be ignored.
- Right click now has the option `Share->Equipment`. After you received equipment information from someone, you can find it once you open their panel, under `Equipment`. You can link these items in chat and see them in the dressing room.
- Right click now has the option `Share->Progress`. After you received progress information from someone, you can find it once you open their panel, under `Progress`. 
- Scrolling in friend panel is now more snappy
- Settings panel added. Also implemented it into memory.
- General ui polish.
- You can now search friends.
- You can change your online status if you want to hide.
- You can now see how many friends you have and how many are online.
- Added sunwell to the progression list.
- Added messages when people come online and go offline
- Added messages for when receiving progress info 
- Ping system changed to ensure we don't over-ping, this prevents limiting outgoing messages in all channels 