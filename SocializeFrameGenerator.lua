SocializeFrameGenerator = {}

function SocializeFrameGenerator.basicMainFrame(parent, bottomRightParent, backdropColor, borderColor)
    local frame = CreateFrame("Frame", nil, parent)
    frame:SetPoint("TOPLEFT", parent, "BOTTOMLEFT")
    frame:SetPoint("BOTTOMRIGHT", bottomRightParent, "BOTTOMRIGHT")
    --tabFrames[i]:SetAllPoints(true)
    frame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });
    if backdropColor then
        frame:SetBackdropColor(unpack(backdropColor))
    else
        frame:SetBackdropColor(0, 0, 0, 1)
    end
    if borderColor then
        frame:SetBackdropBorderColor(unpack(borderColor))
    end
    return frame
end

function SocializeFrameGenerator.mainFrameWithScrollPanel(parent, bottomRightParent, backdropColor, borderColor)
    local mainFrame = SocializeFrameGenerator.basicMainFrame(parent, bottomRightParent, backdropColor, borderColor)
    mainFrame.scrollFrame = SocializeFrameGenerator.initScrollFrame(mainFrame)
    mainFrame.childFrame = SocializeFrameGenerator.initChildFrame(mainFrame.scrollFrame)
    mainFrame.scrollFrame:SetScrollChild(mainFrame.childFrame)
    mainFrame.scrollbar = SocializeFrameGenerator.initScrollbar(mainFrame.scrollFrame, mainFrame.childFrame)
    mainFrame.anchor = SocializeFrameGenerator.initAnchor(mainFrame.childFrame, mainFrame.scrollbar)
    SocializeFrameGenerator.activateScrollbarListener(mainFrame.scrollbar)
    SocializeFrameGenerator.activateScrollFrameListener(mainFrame.scrollFrame, mainFrame.scrollbar, 35)
    return mainFrame
end

function SocializeFrameGenerator.initScrollFrame(parent)
    local scrollFrame = CreateFrame("ScrollFrame", nil, parent)
    scrollFrame:SetPoint("TOPLEFT", 10, -10)
    scrollFrame:SetPoint("BOTTOMRIGHT", -20, 10)
    scrollFrame:EnableMouseWheel(true)
    return scrollFrame
end

function SocializeFrameGenerator.initChildFrame(parent)
    local childFrame = CreateFrame("Frame", nil, parent)
    childFrame:SetBackdrop({
        bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        tile = true, tileSize = 16, edgeSize = 16,
        insets = { left = 0, right = 0, top = 0, bottom = 0 }
    });
    childFrame:SetBackdropColor(0, 0, 0, 0.5); -- set color, 4th argument is alpha (transparency)
    childFrame:SetSize(325, 1000)
    return childFrame
end

function SocializeFrameGenerator.initScrollbar(scrollFrame, childFrame)
    local scrollbar = CreateFrame("Slider", nil, scrollFrame, "UIPanelScrollBarTemplate")
    scrollbar:SetPoint("TOPRIGHT", scrollFrame, "TOPRIGHT", 16, -10)
    scrollbar:SetPoint("BOTTOMRIGHT", scrollFrame, "BOTTOMRIGHT", 16, 9)
    scrollbar:SetMinMaxValues(0, childFrame:GetHeight() - scrollFrame:GetHeight())
    return scrollbar
end

function SocializeFrameGenerator.initAnchor(parent, scrollBar)
    local settingAnchor = CreateFrame("Frame", nil, parent)
    settingAnchor:SetPoint("TOPLEFT", parent, "TOPLEFT", 0, 0)
    settingAnchor:SetSize(parent:GetWidth() - scrollBar:GetWidth(), 1)
    return settingAnchor
end

function SocializeFrameGenerator.activateScrollbarListener(scrollbar)
    scrollbar:SetScript("OnValueChanged", function (scrollbarSelf, value)
        scrollbarSelf:GetParent():SetVerticalScroll(value)
    end)
end

function SocializeFrameGenerator.activateScrollFrameListener(scrollFrame, scrollbar, stepSize)
    scrollFrame:SetScript("OnMouseWheel", function(_, delta)
        local currentValue = scrollbar:GetValue()
        local changedValue = currentValue - (delta*stepSize)
        scrollbar:SetValue(changedValue)
    end)
end

function SocializeFrameGenerator.initTabFrame(frames, parent)
    local tabs = {}
    local anchor = CreateFrame("Frame", nil, parent)
    anchor:SetSize(1,1)
    anchor:SetPoint("TOPLEFT", 7, -50)
    local firstTitle
    for i = 1, #frames do
        local tabTitle = frames[i][1]
        local frameClass = frames[i][2]
        if not firstTitle then
            firstTitle = tabTitle
        end
        tabs[tabTitle] = {}
        tabs[tabTitle].button = CreateFrame("Button", "tab"..tabTitle, parent, "OptionsFrameTabButtonTemplate")
        tabs[tabTitle].button:SetText(tabTitle)
        tabs[tabTitle].button:SetPoint("TOPLEFT", anchor, "TOPRIGHT", -8, 0)
        anchor = tabs[tabTitle].button
        tabs[tabTitle].frame = frameClass:new(tabs[firstTitle].button, parent)
        tabs[tabTitle].button:SetScript("OnClick", function()
            for j = 1, #frames do
                tabTitleRef = frames[j][1]
                local tabText = _G[tabs[tabTitleRef].button:GetName().."Text"] -- get the FontString
                if tabTitleRef == tabTitle then
                    --tabs[j]:SetBackdropColor(0.2, 0.2, 0.2, 0.7)
                    tabText:SetTextColor(1,0.5,0);
                    tabs[tabTitleRef].frame.mainFrame:Show();
                else
                    --tabs[j]:SetBackdropColor(0,0,0, 0.7);
                    tabText:SetTextColor(1, 0.8, 0.0);
                    tabs[tabTitleRef].frame.mainFrame:Hide();
                end
            end
        end);
    end
    tabs[firstTitle].button:Click();
    return tabs
end

function SocializeFrameGenerator.initCheckbox(label, parent, data, dataKey, tooltipDescription)
    local checkBox = CreateFrame("CheckButton", "checkbox"..label, parent, "ChatConfigCheckButtonTemplate")
    checkBox:SetPoint("TOPLEFT", parent, "BOTTOMLEFT")
    _G[(checkBox:GetName() .. 'Text')]:SetText(label)
    checkBox:SetChecked(data[dataKey])
    checkBox:SetScript("OnClick", function(self)
        data[dataKey] = self:GetChecked()
    end)
    if tooltipDescription then
        checkBox:SetScript("OnEnter", function(self)
            GameTooltip:SetOwner(self, "ANCHOR_RIGHT") -- Attach tooltip to the checkbox
            GameTooltip:SetText(tooltipDescription)
            GameTooltip:Show()
        end)
        -- Script for OnLeave event (mouse hovering ended)
        checkBox:SetScript("OnLeave", function(self)
            GameTooltip:Hide()
        end)
    end
    return checkBox
end

function SocializeFrameGenerator.mainframeWithBackground(parent)
    local mainFrame = CreateFrame("Frame", "Main frame", parent)

    mainFrame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });
    return mainFrame
end

function SocializeFrameGenerator.draggableMainFrame(anchorTo, frameLevel, width, height)
    -- Create a Frame
    local mainFrame = SocializeFrameGenerator.mainframeWithBackground(anchorTo, width, height)
    mainFrame:SetSize(width, height)
    --mainFrame:SetSize(350, 425)

    mainFrame:SetPoint("TOPLEFT", anchorTo, "TOPRIGHT")
    --mainFrame:SetFrameLevel(10)
    mainFrame:SetFrameLevel(frameLevel)
    mainFrame:EnableMouse(true)
    mainFrame:SetMovable(true)
    mainFrame:SetClampedToScreen(true) -- This line ensures the frame won't be dragged off the screen
    mainFrame:RegisterForDrag("LeftButton") -- "LeftButton" makes the frame only draggable by holding down the left mouse button
    mainFrame:SetScript("OnDragStart", mainFrame.StartMoving)
    mainFrame:SetScript("OnDragStop", mainFrame.StopMovingOrSizing)
    return mainFrame
end