SocializeSetIDPopup = {}

---@param friend Friend
function SocializeSetIDPopup.popup(friend)
    StaticPopupDialogs["SOCIALIZE_SET_ID"] = {
        text = "Enter nickname for friend:",
        button1 = "Go",
        button2 = "Back",
        hasEditBox = true,
        OnShow = function(self, data)
            self.editBox:SetText(friend.ID)
            self.editBox:SetFocus()
            self:SetSize(250, 125)
            if not self.checkBox then
                self.checkBox = CreateFrame("CheckButton", nil, self, "UICheckButtonTemplate")
                self.checkBox:SetPoint("BOTTOMRIGHT", self.editBox, "TOPLEFT", -55, -5)
                self.checkBox.text = self:CreateFontString(nil, "ARTWORK", "GameFontNormal")
                self.checkBox.text:SetPoint("LEFT", self.checkBox, "RIGHT", 5, 0)
                self.checkBox.text:SetText("Change name for all related characters/alts")
            end
            self.checkBox:SetChecked(true)
        end,
        OnAccept = function(self)
            local ID = self.editBox:GetText():gsub("^%l", string.upper) --ucfirst
            if ID == "Me" then
                SocializePrint.systemMessage("You cannot set a nickname to Me, as it is reserved for you :/")
                return
            end
            if ID ~= friend.ID then
                local friendRelatives = SocializeStorage.findFriendsByID(friend.ID) --includes friend
                if self.checkBox:GetChecked() then
                    for relativeName, relative in pairs(friendRelatives) do
                        relative.hasManualID = true
                        relative.ID = ID
                    end
                else
                    friend.ID = ID
                end
            end
            --SocializeFriendsFrame.getInstance():updateComponent()
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
        EditBoxOnEnterPressed = function(self)
            local parent = self:GetParent()
            parent.button1:Click()
        end,
    }

    StaticPopup_Show("SOCIALIZE_SET_ID")
end