SocializeFriendConfirmationPopup = {}

function SocializeFriendConfirmationPopup.popup(friendName, friendsFrame)
    StaticPopupDialogs["SOCIALIZE_CONFIRM_FRIEND"] = {
        text = friendName.." wants to be friends, accept?",
        button1 = "Sure",
        button2 = "Nah",
        OnAccept = function()
            local friend = SocializeStorage.addFriend(friendName)
            SocializeFriendsFrame.getInstance():addFriend(friend)
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
    }
    StaticPopup_Show ("SOCIALIZE_CONFIRM_FRIEND")
end

-- How to show the popup
