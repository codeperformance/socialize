---@class SocializeCommunication
SocializeCommunication = {
    eventFrame = {},
    ---@type SocializeFriendsFrame
    friendsFrame = {},
    debug = false,
}

function SocializeCommunication:new(friendsFrame)
    ---@type SocializeCommunication
    local instance = {}
    setmetatable(instance, self)
    SocializeCommunication.__index = self
    instance.friendsFrame = friendsFrame
    instance:initEventFrame()
    instance:activateEventFrameListener()
    return instance
end

function SocializeCommunication:initEventFrame()
    local eventFrame = CreateFrame("Frame")
    eventFrame:RegisterEvent("CHAT_MSG_ADDON")
    self.eventFrame = eventFrame
end

function SocializeCommunication:activateEventFrameListener()
    self.eventFrame:SetScript("OnEvent", SocializeCommunication.eventFrameListener)
end

function SocializeCommunication.eventFrameListener(_, event, subEvent, message, type, sender)
    if not SocializeString.startsWith(subEvent, SocializeCommunicationEventNames.prefix) then
        return -- ignore unknown prefixes
    end

    local friend = SocializeStorage.getFriend(sender)
    if friend and friend.ignored then
        return -- ignored friends cannot interact
    end
    if SocializeCommunication.debug then
        print("RECEIVING", subEvent, sender, message)
    end
    if not friend then
        local autoAcceptFriendRequests = SocializeConfig.default.consent.autoAcceptFriendRequests
        local autoAcceptAltFriendRequests = SocializeConfig.default.consent.autoAcceptAltFriendRequests
        local isAcquaintance = false

        if not autoAcceptFriendRequests and subEvent == SocializeCommunicationEventNames.friendRequestEvent then
            SocializeCommunicationEventHandler.onFriendRequest(sender)
            return
        end
        if autoAcceptFriendRequests or autoAcceptAltFriendRequests then
            isAcquaintance = SocializeCommunicationEventHandler.onAreYouOnlineAndAcquaintance(sender, message)
        else
            return --I dont wanna add new people at all, thank you
        end
        if not isAcquaintance and autoAcceptFriendRequests then
            local newFriend = SocializeStorage.addFriend(sender)
            SocializeFriendsFrame.getInstance():addFriend(newFriend)
            SocializePrint.systemMessage("Automatically added " .. sender)
            return
        end
        return
    end

    if subEvent == SocializeCommunicationEventNames.areYouOnlineEvent then
        SocializeCommunicationEventHandler.onAreYouOnline(friend, message)
    elseif subEvent == SocializeCommunicationEventNames.iAmOnlineEvent then
        SocializeCommunicationEventHandler.onIAmOnline(friend, message)
    elseif subEvent == SocializeCommunicationEventNames.iAmReloadingEvent then
        SocializeCommunicationEventHandler.onIAmReloading(friend, SocializeConfig.default.advanced.extraSecondsToReload)
    elseif subEvent == SocializeCommunicationEventNames.shareEquipmentEvent then
        SocializeCommunicationEventHandler.onShareEquipment(friend, message)
    elseif subEvent == SocializeCommunicationEventNames.shareProgressEvent then
        SocializeCommunicationEventHandler.onShareProgress(friend, message)
    elseif subEvent == SocializeCommunicationEventNames.iGoOfflineEvent then
        SocializeCommunicationEventHandler.onIGoOffline(friend)
    end
end

function SocializeCommunication.pingFriend(friend, pingInterval, longPingInterval)
    if SocializeSession.isSelf(friend.name) then
        friend.online = true
        friend.hasBeenOnline = true
        friend.status = SocializeStorage.getPlayer().status
    else
        if not friend.ignored then
            SocializeCommunicationInternal.sendAreYouOnline(friend.name)
            if doLongCheck then
                SocializeCommunication.checkPingResponse(friend, pingInterval, longPingInterval)
            end
        end
    end
end

function SocializeCommunication.pingFriends(pingInterval, longPingInterval)
    SocializeStorage.updateLocationString()
    for friendName, friend in pairs(SocializeStorage.getFriends()) do
        SocializeCommunication.pingFriend(friend, pingInterval, longPingInterval)
    end
end

function SocializeCommunication.sendGoingOffline()
    for friendName, friend in pairs(SocializeStorage.getFriends()) do
        if not friend.ignored and friend.hasBeenOnline then
            if not SocializeSession.isSelf(friendName) then
                SocializeCommunicationInternal.sendIGoOffline(friend.name)
            end
        end
    end
end

---@param friend Friend
---@param checkAfterSeconds number
function SocializeCommunication.checkLongPingResponse(friend, checkAfterSeconds)
    C_Timer.After(checkAfterSeconds, function()
        if not friend.lastResponse or friend.lastResponse < time() - checkAfterSeconds then
            friend.hasBeenOnline = false
        end
    end)
end

---@param friend Friend
---@param pingInterval number
function SocializeCommunication.checkPingResponse(friend, pingInterval, longPingInterval)
    C_Timer.After(pingInterval , function()
        if not friend.lastResponse or friend.lastResponse < time() - (pingInterval* 1.5) then
            if friend.online then
                local isNotSelf = SocializeSession.isSelf(friend.name)
                local isSocializeFrameVisible = SocializeFrame.instance.mainFrame:IsVisible()
                if isNotSelf and not isSocializeFrameVisible then
                    SocializePrint.systemMessage(friend.name.." has gone offline")
                end
                friend.online = false
            end
            SocializeCommunication.checkLongPingResponse(friend, longPingInterval)
        end
    end)
end

function SocializeCommunication.pingOnlineFriends(pingInterval, lastCheckAfterSeconds)
    --print("PingInterval")
    SocializeStorage.updateLocationString()
    local friends = SocializeStorage.getFriends()
    local onlineFriends = {}
    local onlineFriendCount = 0
    for _, friend in pairs(friends) do
        if not friend.ignored and friend.hasBeenOnline then
            onlineFriendCount = onlineFriendCount + 1
            table.insert(onlineFriends, friend)
        end
    end
    local batchSize = 5
    local delayRatio = math.ceil(onlineFriendCount / batchSize)
    if delayRatio < 1 then
        delayRatio = 1
    end
    if batchSize > onlineFriendCount then
        batchSize = onlineFriendCount
    end
    if batchSize < 1 then
        batchSize = 1
    end
    local delay = (pingInterval - 0.5) / delayRatio
    local iteration = 1
    --print(delay)
    for i = 1, onlineFriendCount, batchSize do
        --print("modified delay "..delay*iteration)
        C_Timer.After(delay*iteration, function()
            for j = i, math.min(i + batchSize - 1, #onlineFriends) do
                local friend = onlineFriends[j]
                if not friend.ignored and friend.hasBeenOnline then
                    if not SocializeSession.isSelf(friend.name) then
                        SocializeCommunicationInternal.sendAreYouOnline(friend.name)
                        SocializeCommunication.checkPingResponse(friend, pingInterval, lastCheckAfterSeconds)
                    else
                        friend.status = SocializeStorage.getPlayer().status
                        friend.location = SocializeStorage.getPlayer().location
                    end
                end
            end
        end)
        iteration = iteration + 1
    end
end
