---@class SocializeConfig
SocializeConfig = {
    restore = {},--will be filled when loading
    default = {
        advanced = {
            show = false,
            extraSecondsToReload = 20,
            pingIntervalInSeconds = 5,
            UIRefreshIntervalInSeconds = 0.03,
            lastCheckAfterSeconds = 30,
            alpha = 0.8,
            debug = false,
        },
        consent = {
            autoSendFriendRequestFromAlts = false,
            autoAcceptAltFriendRequests = false,
            autoSendProgress = false,
            autoAcceptFriendRequests = false,
        },
        colors = {
            socializeFrame = {
                backdropColor = { 0, 0, 0, 1 },
                borderColor = { 1, 1, 1, 1 },
                statusTextColor = { 1, 1, 1, 1 },
                totalFriendsTextColor = { 1, 1, 1, 1 },
            },
            friendFrame = {
                backdropColorOffline = { 0.2, 0.2, 0.2, 1 },
                backdropColorOnline = { 0.3, 0.3, 0, 1 },
                backdropColorIgnored = { 0.3, 0, 0, 1 },
                backdropColorHighlighted = { 0.6, 0.6, 1, 1 },
                borderColor = { 1, 1, 1, 1 },
                selfBorderColor = { 1, 0.55, 0, 1 },
                nameTextColor = { 1, 0.9, 0, 1 },
                locationTextColor = { 0.8, 0.8, 0.8, 1 },
                statusTextColorNormal = { 0.8, 0.8, 0.8, 1 },
                statusTextColorReloading = { 1, 0.5, 0, 1 },
            },
            friendInfoFrame = {
                backdropColor = { 0, 0, 0, 1 },
                borderColor = { 1, 1, 1, 1 },
                nameTextColor = { 1, 1, 1, 1 },
                statusTextColor = { 1, 1, 1, 1 },
                locationTextColor = { 1, 1, 1, 1 },
                equipment = {
                    backdropColor = { 0, 0, 0, 1 },
                    borderColor = { 1, 1, 1, 1 },
                    unavailable = { 0.3, 0.3, 0.3, 1 },
                    uncommon = { 0.1176, 1, 0, 1 },
                    rare = { 0, 0.4392, 0.8667, 1 },
                    epic = { 0.6392, 0.2078, 0.9333, 1 },
                    legendary = { 0.98, 0.52, 0.24, 1 },
                    vanity = { 0.9, 0.8, 0.5, 1 },
                    heirloom = { 0.9, 0.8, 0.5, 1 },
                },
                progress = {
                    backdropColor = { 0, 0, 0, 1 },
                    borderColor = { 1, 1, 1, 1 },
                    progressItem = {
                        backdropColor = { 0, 0, 0, 1 },
                        borderColor = { 1, 1, 1, 1 },
                        labelTextColor = { 1, 1, 1, 1 },
                        titleTextColor = { 1, 1, 1, 1 },
                        Missing = { 0.3, 0.3, 0.3, 1 },
                        Normal = { 0.1176, 1, 0, 1 },
                        Heroic = { 0, 0.4392, 0.8667, 1 },
                        Mythic = { 0.6392, 0.2078, 0.9333, 1 },
                        Ascended = { 0.98, 0.52, 0.24, 1 },
                    },
                },
            }
        },
    }
}

function SocializeConfig.initialize()
    if not Socialize["config"] then
        SocializeConfig.storeConfig()
    else
        SocializeConfig.loadConfig()
    end
end

function SocializeConfig.storeConfig()
    Socialize["config"] = SocializeConfig.default
end

function SocializeConfig.loadConfig()
    SocializeConfig.restore = SocializeConfig.deepCopy(SocializeConfig.default)
    SocializeConfig.updateSegments(SocializeConfig.default, Socialize["config"])
    SocializeConfig.default = Socialize["config"]
end

function SocializeConfig.updateSegments(fromSegment, toSegment)
    for key, value in pairs(fromSegment) do
        if not toSegment[key] then
            toSegment[key] = value
        elseif type(value) == 'table' then
            SocializeConfig.updateSegments(fromSegment[key], toSegment[key])
        end
    end
end

function SocializeConfig.deepCopy(originalTable)
    local copyOfTable = {}
    for key, value in pairs(originalTable) do
        if type(value) == "table" then
            copyOfTable[key] = SocializeConfig.deepCopy(value)
        else
            copyOfTable[key] = value
        end
    end
    return copyOfTable
end

function SocializeConfig.reset()
    SocializeConfig.recursiveSet(SocializeConfig.restore, SocializeConfig.default)
end

function SocializeConfig.recursiveSet(fromSegment, toSegment)
    for key, value in pairs(fromSegment) do
        if type(value) == 'table' then
            SocializeConfig.recursiveSet(fromSegment[key], toSegment[key])
        else
            toSegment[key] = value
        end
    end
end
