SocializeFriendRemovePopup = {}

function SocializeFriendRemovePopup.popup(friendName)
    StaticPopupDialogs["SOCIALIZE_CONFIRM_REMOVE"] = {
        text = "Are you sure you want to remove "..friendName.."? This will be done account-wide!",
        button1 = "Sure",
        button2 = "Nah",
        OnAccept = function()
            SocializeStorage.removeFriend(friendName)
            SocializeFriendsFrame.getInstance():getFriendFrame(friendName):remove()
            --SocializeFriendsFrame.getInstance():updateComponent()
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
    }
    StaticPopup_Show ("SOCIALIZE_CONFIRM_REMOVE")
end
