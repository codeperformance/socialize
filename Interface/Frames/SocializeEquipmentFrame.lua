---@class SocializeEquipmentFrame
SocializeEquipmentFrame = {
    ---@type SocializeFriendInfoFrame
    friendInfoFrame = nil,
    mainFrame = nil,
    texture = nil,
    border = nil,
    ---@type SocializeItemFrame[]
    equipmentItemFrames = {},
    leftColumnEquipmentSlots = {
        "head",
        "neck",
        "shoulder",
        "back",
        "chest",
        "shirt",
        "tabard",
        "wrist",
    },
    rightColumnEquipmentSlots = {
        "hands",
        "waist",
        "legs",
        "feet",
        "finger1",
        "finger2",
        "trinket1",
        "trinket2",
    },
    bottomRowEquipmentSlots = {
        "mainhand",
        "offhand",
        "ranged",
    },
}

function SocializeEquipmentFrame:new(parent, anchorPoint)
    --- @type SocializeEquipmentFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeEquipmentFrame.__index = self
    instance.friendInfoFrame = parent
    instance:initMainFrame(parent.mainFrame, anchorPoint)
    instance:initLeftColumn(12, -47, 36)
    instance:initRightColumn(-12, -47, 36)
    instance:initBottomRow(-42, 12, 36)
    return instance
end

function SocializeEquipmentFrame:initMainFrame(parent, anchorPoint)
    local frame = CreateFrame("Frame", nil, parent)
    frame:SetPoint("TOPLEFT", anchorPoint, "BOTTOMLEFT")
    frame:SetPoint("BOTTOMRIGHT", parent, "BOTTOMRIGHT")
    --tabFrames[i]:SetAllPoints(true)
    frame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });
    self.mainFrame = frame
end

function SocializeEquipmentFrame:initLeftColumn(x, y, frameInterval)
    for i = 1, #self.leftColumnEquipmentSlots do
        local itemFrame = SocializeItemFrame:new(self, self.leftColumnEquipmentSlots[i], "TOPLEFT", x, y - ((i-1)* frameInterval))
        self.equipmentItemFrames[self.leftColumnEquipmentSlots[i]] = itemFrame
    end
end

function SocializeEquipmentFrame:initRightColumn(x, y, frameInterval)
    for i = 1, #self.rightColumnEquipmentSlots do
        local itemFrame = SocializeItemFrame:new(self, self.rightColumnEquipmentSlots[i], "TOPRIGHT", x, y - ((i-1)* frameInterval))
        self.equipmentItemFrames[self.rightColumnEquipmentSlots[i]] = itemFrame
    end
end

function SocializeEquipmentFrame:initBottomRow(x, y, frameInterval)
    for i = 1, #self.bottomRowEquipmentSlots do
        local itemFrame = SocializeItemFrame:new(self, self.bottomRowEquipmentSlots[i], "BOTTOM", x + ((i-1)* frameInterval), y)
        self.equipmentItemFrames[self.bottomRowEquipmentSlots[i]] = itemFrame
    end
end

function SocializeEquipmentFrame:updateComponent()
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendInfoFrame.equipment.backdropColor))
    self.mainFrame:SetBackdropBorderColor(unpack(SocializeConfig.default.colors.friendInfoFrame.equipment.borderColor))
    for slotName, itemFrame in pairs(self.equipmentItemFrames) do
        itemFrame:updateComponent()
    end
end


