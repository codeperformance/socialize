## Interface: 30300 
## Title: Socialize
## Notes: Friend list on steroids
## Version: 3.0
## Author: Hipnotize
## SavedVariables: Socialize

#Config
SocializeConfig.lua

# Utilities
SocializeSession.lua
SocializeFrameGenerator.lua
SocializePrint.lua
SocializeGlobalHelper.lua
SocializeString.lua
SocializeFriendWrapper.lua
SocializeStorage.lua
SocializeProgressType.lua
SocializeProgress.lua
SocializeEquipmentSlotMapping.lua
SocializeCommunicationEventNames.lua
SocializeCommunicationInternal.lua
SocializeCommunicationEventHandler.lua
SocializeCommunication.lua
SocializeCore.lua

# Popups
Interface\Popups\SocializeSetIDPopup.lua
Interface\Popups\SocializeShareProgressPopup.lua
Interface\Popups\SocializeFriendAddPopup.lua
Interface\Popups\SocializeShareEquipmentPopup.lua
Interface\Popups\SocializeFriendConfirmationPopup.lua
Interface\Popups\SocializeFriendRemovePopup.lua
Interface\Popups\SocializeFriendIgnorePopup.lua
Interface\Popups\SocializeStatusUpdatePopup.lua
Interface\Popups\SocializeResetSettingsPopup.lua
Interface\Popups\SocializeAboutPopup.lua

# Frames
Interface\Frames\SocializeEquipmentFrame.lua
Interface\Frames\SocializeProgressFrame.lua
Interface\Frames\SocializeItemFrame.lua
Interface\Frames\SocializeFriendInfoFrame.lua
Interface\Frames\SocializeFriendsFrame.lua
Interface\Frames\SocializeFriendFrame.lua
Interface\Frames\SocializeFrame.lua
Interface\Frames\SocializeProgressItemFrame.lua
Interface\Frames\SocializeSettingsFrame.lua
Interface\Frames\SocializeSettingsColorOptionFrame.lua
Interface\Frames\SocializeSettingsColorFrame.lua
Interface\Frames\SocializeSettingsConsentFrame.lua
Interface\Frames\SocializeSettingsAdvancedFrame.lua

# Main
Socialize.lua