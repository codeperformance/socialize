SocializeProgress = {
    mapping = { --All progress options relevant for now
        tier6 = {
            chest = { OrderID = 1, Normal = 70001, Heroic = 70009, Mythic = 70017, Ascended = 70025 },
            legs = { OrderID = 2, Normal = 70002, Heroic = 70010, Mythic = 70018, Ascended = 70026 },
            head = { OrderID = 3, Normal = 70003, Heroic = 70011, Mythic = 70019, Ascended = 70027 },
            shoulder = { OrderID = 4, Normal = 70004, Heroic = 70012, Mythic = 70020, Ascended = 70028 },
            gloves = { OrderID = 5, Normal = 70005, Heroic = 70013, Mythic = 70021, Ascended = 70029 },
            bracer = { OrderID = 6, Normal = 70006, Heroic = 70014, Mythic = 70022, Ascended = 70030 },
            belt = { OrderID = 7, Normal = 70007, Heroic = 70015, Mythic = 70023, Ascended = 70031 },
            feet = { OrderID = 8, Normal = 70008, Heroic = 70016, Mythic = 70024, Ascended = 70032 },
        },
        tier5 = {
            chest = { OrderID = 1, Normal = 47021, Heroic = 47026, Mythic = 47036, Ascended = 47031 },
            legs = { OrderID = 2, Normal = 47022, Heroic = 47027, Mythic = 47037, Ascended = 47032 },
            head = { OrderID = 3, Normal = 47023, Heroic = 47028, Mythic = 47038, Ascended = 47033 },
            shoulder = { OrderID = 4, Normal = 47024, Heroic = 47029, Mythic = 47039, Ascended = 47034 },
            gloves = { OrderID = 5, Normal = 47025, Heroic = 47030, Mythic = 47040, Ascended = 47035 },
        },
        blackTemple = {
            najentus = { OrderID = 1, Normal = 11937, Heroic = 12037, Mythic = 27137, Ascended = 112137 },
            supremus = { OrderID = 2, Normal = 11938, Heroic = 12038, Mythic = 27138, Ascended = 112138 },
            shade = { OrderID = 3, Normal = 11939, Heroic = 12039, Mythic = 27139, Ascended = 112139 },
            gorefiend = { OrderID = 4, Normal = 11940, Heroic = 12040, Mythic = 27140, Ascended = 112140 },
            bloodboil = { OrderID = 5, Normal = 11941, Heroic = 12041, Mythic = 27141, Ascended = 112141 },
            reliquary = { OrderID = 6, Normal = 11942, Heroic = 12042, Mythic = 27142, Ascended = 112142 },
            mother = { OrderID = 7, Normal = 11943, Heroic = 12043, Mythic = 27143, Ascended = 112143 },
            council = { OrderID = 8, Normal = 11944, Heroic = 12044, Mythic = 27144, Ascended = 112144 },
            illidan = { OrderID = 9, Normal = 11948, Heroic = 12048, Mythic = 27148, Ascended = 112148 },
        },
        mountHyjal = {
            winterchill = { OrderID = 1, Normal = 11932, Heroic = 12032, Mythic = 27132, Ascended = 112132 },
            anetheron = { OrderID = 2, Normal = 11933, Heroic = 12033, Mythic = 27133, Ascended = 112133 },
            kazrogal = { OrderID = 3, Normal = 11934, Heroic = 12034, Mythic = 27134, Ascended = 112134 },
            azgalor = { OrderID = 4, Normal = 11935, Heroic = 12035, Mythic = 27135, Ascended = 112135 },
            chromius = { OrderID = 5, Normal = 21056, Heroic = 21057, Mythic = 21058, Ascended = 21059 },
            archimonde = { OrderID = 6, Normal = 11936, Heroic = 12036, Mythic = 27136, Ascended = 112136 },
        },
        sunwellPlateau = {
            kalecgos = { OrderID = 1, Normal = 11949, Heroic = 12049, Mythic = 27149, Ascended = 12149 },
            brutallus = { OrderID = 2, Normal = 11951, Heroic = 12051, Mythic = 27151, Ascended = 12151 },
            felmyst = { OrderID = 3, Normal = 11952, Heroic = 12052, Mythic = 27152, Ascended = 12152 },
            arynyes = { OrderID = 4, Normal = 11953, Heroic = 12053, Mythic = 27153, Ascended = 12153 },
            --arynyes = { OrderID = 4, Normal = 11950, Heroic = 12050, Mythic = 27150, Ascended = 12150 }, --Removed? Boss not available
            twins = { OrderID = 5, Normal = 11954, Heroic = 12054, Mythic = 27154, Ascended = 12154 },
            muru = { OrderID = 6, Normal = 11955, Heroic = 12055, Mythic = 27155, Ascended = 12155 },
            kiljaeden = { OrderID = 7, Normal = 11956, Heroic = 12056, Mythic = 27156, Ascended = 12156 },
        },
        zulAman = {
            nolarakk = { OrderID = 1, Normal = 11909, Heroic = 12009, Mythic = 27109, Ascended = 112109 },
            janalai = { OrderID = 2, Normal = 11910, Heroic = 12010, Mythic = 27110, Ascended = 112110 },
            akilzon = { OrderID = 3, Normal = 11911, Heroic = 12011, Mythic = 27111, Ascended = 112111 },
            halazzi = { OrderID = 4, Normal = 11912, Heroic = 12012, Mythic = 27112, Ascended = 112112 },
            hexlordmalacrass = { OrderID = 5, Normal = 11913, Heroic = 12013, Mythic = 27113, Ascended = 112113 },
            zuljin = { OrderID = 6, Normal = 11914, Heroic = 12014, Mythic = 27114, Ascended = 112114 },
        },
        serpentShrineCavern = {
            hydross = { OrderID = 1, Normal = 11922, Heroic = 12022, Mythic = 27122, Ascended = 112122 },
            lurker = { OrderID = 2, Normal = 11923, Heroic = 12023, Mythic = 27123, Ascended = 112123 },
            leotheras = { OrderID = 3, Normal = 11924, Heroic = 12024, Mythic = 27124, Ascended = 112124 },
            karathress = { OrderID = 4, Normal = 11925, Heroic = 12025, Mythic = 27125, Ascended = 112125 },
            morogrim = { OrderID = 5, Normal = 11926, Heroic = 12026, Mythic = 27126, Ascended = 112126 },
            vashj = { OrderID = 6, Normal = 11927, Heroic = 12027, Mythic = 27127, Ascended = 112127 },
        },
        tempestKeep = {
            alar = { OrderID = 1, Normal = 11928, Heroic = 12028, Mythic = 27128, Ascended = 112128 },
            voidreaver = { OrderID = 2, Normal = 11929, Heroic = 12029, Mythic = 27129, Ascended = 112129 },
            solarian = { OrderID = 3, Normal = 11930, Heroic = 12030, Mythic = 27130, Ascended = 112130 },
            kealthas = { OrderID = 4, Normal = 11931, Heroic = 12031, Mythic = 27131, Ascended = 112131 },
        },
        gruulsLair = {
            maulgar = { OrderID = 1, Normal = 11915, Heroic = 12015, Mythic = 27115, Ascended = 112115 },
            gruul = { OrderID = 2, Normal = 11920, Heroic = 12020, Mythic = 27120, Ascended = 112120 },
        },
        magtheridonsLair = {
            magtheridon = { OrderID = 1, Normal = 11921, Heroic = 12021, Mythic = 27121, Ascended = 112121 },
        },
        karazhan = {
            attumen = { OrderID = 1, Normal = 11900, Heroic = 12000, Mythic = 27100, Ascended = 112100 },
            moroes = { OrderID = 2, Normal = 11901, Heroic = 12001, Mythic = 27101, Ascended = 112101 },
            maiden = { OrderID = 3, Normal = 11902, Heroic = 12002, Mythic = 27102, Ascended = 112102 },
            curator = { OrderID = 4, Normal = 11903, Heroic = 12003, Mythic = 27103, Ascended = 112103 },
            terestian = { OrderID = 5, Normal = 11904, Heroic = 12004, Mythic = 27104, Ascended = 112104 },
            aran = { OrderID = 6, Normal = 11905, Heroic = 12005, Mythic = 27105, Ascended = 112105 },
            netherspite = { OrderID = 7, Normal = 11906, Heroic = 12006, Mythic = 27106, Ascended = 112106 },
            nightbane = { OrderID = 8, Normal = 11907, Heroic = 12007, Mythic = 27107, Ascended = 112107 },
            malchezaar = { OrderID = 9, Normal = 11908, Heroic = 12008, Mythic = 27108, Ascended = 112108 },
        },
    },
    releasedProgressTypes = { --All progress options released in socialize
        SocializeProgressType.tier6,
        SocializeProgressType.tier5,
        SocializeProgressType.blackTemple,
        SocializeProgressType.mountHyjal,
        SocializeProgressType.sunwellPlateau,
        SocializeProgressType.zulAman,
        SocializeProgressType.serpentShrineCavern,
        SocializeProgressType.tempestKeep,
        SocializeProgressType.gruulsLair,
        SocializeProgressType.magtheridonsLair,
        SocializeProgressType.karazhan,
    }
}

function SocializeProgress.getAchievementIds()
    local achievementIds = {}
    for _, section in pairs(SocializeProgress.mapping) do
        for _, achievements in pairs(section) do
            for type, achievementId in pairs(achievements) do
                if type ~= "OrderID" then
                    achievementIds[achievementId] = achievementId
                end
            end
        end
    end
    return achievementIds
end

function SocializeProgress.getMapping(progressType)
    return SocializeProgress.mapping[progressType]
end

function SocializeProgress.getProgress(progressType)
    local progressInfo = {}
    local mapping = SocializeProgress.getMapping(progressType)
    for progressKey, progressMapping in pairs(mapping) do
        progressInfo[progressKey] = SocializeProgress.returnHighestDifficulty(progressMapping)
    end
    return progressInfo
end

function SocializeProgress.returnHighestDifficulty(progressMapping)
    local difficulties = { --ordered from high to low so the loop will exit highest match
        "Ascended",
        "Mythic",
        "Heroic",
        "Normal"
    }
    for i, difficulty in ipairs(difficulties) do
        local _, _, _, completed = GetAchievementInfo(progressMapping[difficulty])
        if completed then
            return difficulty
        end
    end
    return "Missing"
end