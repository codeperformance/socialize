SocializeProgressType = {
    tier6 = "tier6",
    tier5 = "tier5",
    sunwellPlateau = "sunwellPlateau",
    blackTemple = "blackTemple",
    mountHyjal = "mountHyjal",
    zulAman = "zulAman",
    serpentShrineCavern = "serpentShrineCavern",
    tempestKeep = "tempestKeep",
    gruulsLair = "gruulsLair",
    magtheridonsLair = "magtheridonsLair",
    karazhan = "karazhan",
}