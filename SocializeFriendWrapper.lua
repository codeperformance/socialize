---@class SocializeFriendWrapper
SocializeFriendWrapper = {
    ---@type Friend
    friend = {},
    ---@type table<string, Friend>
    references = {}
}

function SocializeFriendWrapper:new(friend)
    if  SocializeFriendWrapper.references[friend.name] ~= nil then
        return SocializeFriendWrapper.references[friend.name]
    end
    ---@type SocializeFriendWrapper
    local instance = {}
    setmetatable(instance, self)
    SocializeFriendWrapper.__index = self
    instance.friend = friend
    SocializeFriendWrapper.initializeID(friend)
    SocializeFriendWrapper.initializeMetadata(friend)
    SocializeFriendWrapper.initializeVerifiedAs(friend)
    SocializeFriendWrapper.initializeEquipment(friend)
    SocializeFriendWrapper.initializeProgress(friend)
    SocializeFriendWrapper.references[friend.name] = instance
    return instance
end
---@param friendName string
function SocializeFriendWrapper.removeReference(friendName)
    if  SocializeFriendWrapper.references[friendName] ~= nil then
        SocializeFriendWrapper.references[friendName] = nil
    end
end

---@return Friend
function SocializeFriendWrapper:getFriend()
    return self.friend
end

function SocializeFriendWrapper.newFriendTable(friendName, ID, CustomStatus)
    ---@class Friend
    local friend = {
        name = friendName,
        online = false,
        status = CustomStatus or "Not accepted yet",
        location = "",
        lastResponse = 0,
        ID = ID,
        hasManualID = ID ~= nil,
        ---@type table<string, FriendRelationship>
        verifiedAs = {
            --["Hipnotize"] = {
            --    localKey = "RANDOM1",
            --    foreignKey = "RANDOM2"
            --},
            --["Hipno"] = {
            --    localKey = "RANDOM3",
            --    foreignKey = "RANDOM4"
            --}
        },
        ignored = false,
        hasBeenOnline = true, --to ensure we ping a couple of times to check online status
        metadata = {},
    }
    SocializeFriendWrapper.initializeID(friend)
    SocializeFriendWrapper.initializeEquipment(friend)
    SocializeFriendWrapper.initializeProgress(friend)
    return friend
end

---@param friend Friend
function SocializeFriendWrapper.initializeMetadata(friend)
    if not friend.metadata then
        friend.metadata = {}
    end
end

---@param friend Friend
function SocializeFriendWrapper.initializeID(friend)
    if not friend.ID then
        friend.ID = SocializeString.randomAlphabeticString(16)
    end
    if not friend.hasManualID then
        friend.hasManualID = false
    end
end

---@param friend Friend
function SocializeFriendWrapper.initializeVerifiedAs(friend)
    if not friend.verifiedAs then
        friend.verifiedAs = {}
    end
end

---@param friend Friend
function SocializeFriendWrapper.initializeEquipment(friend)
    if not friend.metadata.equipment then
        friend.metadata.equipment = {}
        for slotName, slotID in pairs(SocializeEquipmentSlotMapping) do
            friend.metadata.equipment[slotName] = ""
        end
    end
end

---@param friend Friend
function SocializeFriendWrapper.initializeProgress(friend)
    if not friend.metadata.progress then
        friend.metadata.progress = {}
    end
    for progressKey, _ in pairs(SocializeProgressType) do
        if not friend.metadata.progress[progressKey] then
            friend.metadata.progress[progressKey] = {}
        end
    end
end

function SocializeFriendWrapper:addProgress(achievementID, completed)
    self.metadata.progress[achievementID] = completed
end

function SocializeFriendWrapper:getProgress(achievementID)
    return self.metadata.progress[achievementID]
end

function SocializeFriendWrapper:getEquipmentItemLink(slotName)
    local equipmentSlot = self.friend.metadata.equipment[slotName]
    if equipmentSlot == "" then
        return false
    end
    return equipmentSlot
end

function SocializeFriendWrapper:setEquipmentItemLink(slotName, itemLink)
    self.friend.metadata.equipment[slotName] = itemLink
end

function SocializeFriendWrapper:hasEquipmentInSlot(slotName)
    return self.friend.metadata.equipment[slotName] and self.friend.metadata.equipment[slotName] ~= ""
end

function SocializeFriendWrapper:getProgress(progressType)
    return self.friend.metadata.progress[progressType]
end

function SocializeFriendWrapper:setProgress(progressType, slotName, difficulty)
    self.friend.metadata.progress[progressType][slotName] = difficulty
end

function SocializeFriendWrapper:getLocalKey()
    if not self.friend.localKey then
        self.friend.localKey = SocializeString.randomAlphabeticString(16)
    end
    return self.friend.localKey
end
---@param foreignKey string|nil
function SocializeFriendWrapper:addVerifiedAs()
    if self.friend.verifiedAs[SocializeSession.playerName] ~= nil then
        return
    end
    ---@class FriendRelationship
    local friendRelationship = {
        localKey = SocializeString.randomAlphabeticString(16),
        foreignKey = nil,
    }
    self.friend.verifiedAs[SocializeSession.playerName] = friendRelationship
end

function SocializeFriendWrapper:updateVerifiedAsForeignKey(foreignKey)
    local playerName = SocializeSession.playerName
    if self.friend.verifiedAs[playerName] then
        self.friend.verifiedAs[playerName].foreignKey = foreignKey
    end
end

function SocializeFriendWrapper:isVerified()
    for altName, relationship in pairs(self.friend.verifiedAs) do
        if relationship.foreignKey then
            return true
        end
    end
    return false
end

function SocializeFriendWrapper:getForeignKey()
    return self.friend.foreignKey
end


function SocializeFriendWrapper:createIdentityPackage()
    local playerName = SocializeSession.playerName
    self:addVerifiedAs()
    local returnString = ""
    if SocializeConfig.default.consent.autoSendFriendRequestFromAlts
            and not self.friend.verifiedAs[playerName].foreignKey
            and SocializeGlobalHelper.getTableLength(self.friend.verifiedAs) > 1
    then
        for altName, altRelationship in pairs(self.friend.verifiedAs) do
            if altName ~= playerName and altRelationship.foreignKey then
                returnString = altName.."\1"..altRelationship.foreignKey.."\1"
                break
            end
        end
    end
    returnString = returnString..self.friend.verifiedAs[playerName].localKey
    return returnString
end





