---@class SocializeSettingsFrame
SocializeSettingsFrame = {
    mainFrame = {},
    ---@type SocializeFrame
    socializeFrame = nil,
    ---@type SocializeSettingsFrame
    instance = nil,
    scrollFrame = {},
    childFrame = {},
    scrollbar = {},
    anchor = {},
    tabs = {},
    tabFrames = {},
    ---@type SocializeSettingsColorOptionFrame[]
    colorOptions = {},
    resetColorButton = {},
}

function SocializeSettingsFrame.getInstance()
    return SocializeFrame.instance
end

function SocializeSettingsFrame:new(socializeFrame)
    if SocializeSettingsFrame.instance then
        return SocializeSettingsFrame.instance
    end
    ---@type SocializeSettingsFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeSettingsFrame.__index = self
    instance.socializeFrame = socializeFrame
    instance:initMainFrame()
    instance:initResetButton()
    local tabs = {
        { "Consent", SocializeSettingsConsentFrame },
        { "Color", SocializeSettingsColorFrame },
    }
    --These options are not useful to casual players
    if SocializeConfig.default.advanced.show then
        table.insert(tabs, { "Advanced", SocializeSettingsAdvancedFrame })
    end
    instance.tabs = SocializeFrameGenerator.initTabFrame(tabs, instance.mainFrame)
    instance:initTitleTextElement()
    SocializeSettingsFrame.instance = instance
    return instance
end

function SocializeSettingsFrame:initMainFrame()
    -- Create a Frame
    local mainFrame = CreateFrame("Frame", "Main frame",  self.socializeFrame.mainFrame)
    mainFrame:SetSize(350, 425)
    mainFrame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });
    mainFrame:SetBackdropColor(0, 0, 0, 1); -- set background color
    mainFrame:SetBackdropBorderColor(1, 1, 1, 1); -- set border color
    local CloseButton = CreateFrame("Button", nil, mainFrame, "UIPanelCloseButton")
    CloseButton:SetPoint("TOPRIGHT", mainFrame, "TOPRIGHT")
    mainFrame:SetPoint("TOPLEFT", self.socializeFrame.mainFrame, "TOPRIGHT", 10, 0)
    mainFrame:SetFrameLevel(40)
    mainFrame:EnableMouse(true)
    mainFrame:SetMovable(true)
    mainFrame:SetClampedToScreen(true) -- This line ensures the frame won't be dragged off the screen
    mainFrame:RegisterForDrag("LeftButton") -- "LeftButton" makes the frame only draggable by holding down the left mouse button
    mainFrame:SetScript("OnDragStart", mainFrame.StartMoving)
    mainFrame:SetScript("OnDragStop", mainFrame.StopMovingOrSizing)
    mainFrame:Hide()
    self.mainFrame = mainFrame
end

function SocializeSettingsFrame:initResetButton()
    local resetColorButton = CreateFrame("Button", nil, self.mainFrame, "GameMenuButtonTemplate")
    resetColorButton:SetSize(80 , 20)
    resetColorButton:SetPoint("TOPRIGHT", -30, -5)
    resetColorButton:SetText("Reset")
    resetColorButton:SetScript("OnClick", function()
        SocializeResetSettingsPopup.popup()
    end)
    self.resetColorButton = resetColorButton
end

function SocializeSettingsFrame:initTitleTextElement()
    local titleString = self.mainFrame:CreateFontString("TitleText", "OVERLAY")
    titleString:SetPoint("TOPLEFT", self.mainFrame, "TOPLEFT", 10, -10) -- This positions the text in the center of the frame
    local _, _, location_font_style = titleString:GetFont()
    titleString:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style)
    titleString:SetTextColor(1, 1, 1)
    titleString:SetText("Settings:")
    self.mainFrame.text1 = titleString
    self.titleTextElement = titleString
end


function SocializeSettingsFrame:updateComponent()
    for _, tab in pairs(self.tabs) do
        tab.frame:updateComponent()
    end
end
