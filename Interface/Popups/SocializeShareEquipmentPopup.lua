SocializeShareEquipmentPopup = {}

function SocializeShareEquipmentPopup.popup(friendName)
    StaticPopupDialogs["SOCIALIZE_SHARE_EQUIPMENT"] = {
        text = "Are you sure you want to share your current equipment with "..friendName.."?",
        button1 = "Jow",
        button2 = "Uhh no",
        hasEditBox = false,
        OnAccept = function(self)
            SocializeCommunicationInternal.sendAllEquipment(friendName)
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
    }

    StaticPopup_Show("SOCIALIZE_SHARE_EQUIPMENT")
end
