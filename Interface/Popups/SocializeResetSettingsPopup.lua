SocializeResetSettingsPopup = {}

function SocializeResetSettingsPopup.popup()
    StaticPopupDialogs["SOCIALIZE_RESET_SETTINGS"] = {
        text = "Are you sure you want to reset all your settings?", -- this is the prompt text
        button1 = "Add", -- confirm button
        button2 = "Cancel", -- cancel button
        OnAccept = function()
            SocializeConfig.reset()
        end,
        timeout = 0, -- popup will remain until manually closed
        whileDead = true, -- popup can be shown while dead
        hideOnEscape = true, -- hides pop-up when ESC is pressed
        preferredIndex = 3,  -- avoid taint ui issues by using unique indices
    }

    -- How to show the popup
    StaticPopup_Show ("SOCIALIZE_RESET_SETTINGS")
end
