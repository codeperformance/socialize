SocializeGlobalHelper = {}

function SocializeGlobalHelper.getTableLength(tableObject, filter)
    local count = 0
    for _,row in pairs(tableObject) do
        if filter and type(filter) == "function" then
            if filter(row) then
                count = count + 1
            end
        else
            count = count + 1
        end
    end
    return count
end

function SocializeGlobalHelper.filterTable(tableObject, filter)
    local result = {}
    for key,row in pairs(tableObject) do
        if filter(row) then
            result[key] = row
        end
    end
    return result
end

function SocializeGlobalHelper.existsInTable(tableObject, value)
    for _,row in pairs(tableObject) do
        if row == value then
            return true
        end
    end
    return false
end

function SocializeGlobalHelper.hasAura(auraID)
    for i = 1, 40 do
        local currentAuraID = select(11, UnitAura("player", i))
        if auraID == currentAuraID then
            return true
        end
    end
    return false
end