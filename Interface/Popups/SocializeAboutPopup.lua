SocializeAboutPopup = {}

function SocializeAboutPopup.popup()
    StaticPopupDialogs["SOCIALIZE_ABOUT"] = {
        text = "Version: "..GetAddOnMetadata("Socialize", "Version").."\n\nMade by Hipnotize.\n\nWanna contribute or report bug?\nPoke me in discord(hipnotize)",
        button1 = "Alrighty then",
        hasEditBox = false,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
    }

    StaticPopup_Show("SOCIALIZE_ABOUT")
end

