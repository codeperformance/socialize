---@class SocializeFrame
SocializeFrame = {
    mainFrame = {},
    toggleButton = {},
    addFriendButton = {},
    setStatusButton = {},
    settingsButton = {},
    aboutButton = {},
    statusTextElement = {},
    ---@type SocializeFriendsFrame
    friendsFrame = {},
    ---@type SocializeFriendInfoFrame
    friendInfoFrame = {},
    ---@type SocializeSettingsFrame
    settingsFrame = {},
    ---@type SocializeFrame
    instance = nil,
    searchBox = {},
    metaTextElement = {},
}

function SocializeFrame.getInstance()
    return SocializeFrame.instance
end

function SocializeFrame:new(anchorTo)
    if SocializeFrame.instance then
        return SocializeFrame.instance
    end
    ---@type SocializeFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeFrame.__index = self
    instance:initMainFrame(anchorTo)
    instance:initToggleButton(anchorTo)
    instance:initAddFriendButton()
    instance:initSetStatusButton()
    instance:initStatusTextElement()
    instance:initSettingsButton()
    instance:initAboutButton()
    instance:initFriendMetaTextElements()
    instance:initSearchBox()
    instance:activateAddFriendButtonListener()
    instance:activateSetStatusButtonListener()
    instance:activateSettingsButtonListener()
    instance:activateAboutButtonListener()
    --instance:initScrollFrame()
    --instance:initChildFrame()
    --instance:initScrollbar()
    --instance:initFriendAnchor()
    --instance:activateScrollbarListener()
    --instance:activateScrollFrameListener()
    instance.friendsFrame = SocializeFriendsFrame:new(instance)
    instance.friendInfoFrame = SocializeFriendInfoFrame:new(instance)
    instance.settingsFrame = SocializeSettingsFrame:new(instance)
    instance:activateOnShowEventListener()
    instance:activateOnTextChangeEventListener()
    instance:initOnlineStatusDropdown()
    SocializeFrame.instance = instance
    return instance
end

function SocializeFrame:togglePanel(frame, toggleTo)
    if toggleTo then
        frame:Show()
    else
        frame:Hide()
    end
end

function SocializeFrame:initToggleButton(anchorTo)
    local toggleButton = CreateFrame("Button", nil, anchorTo, "GameMenuButtonTemplate")
    toggleButton:SetPoint("TOPRIGHT", anchorTo, "TOPRIGHT", -55, -13)
    toggleButton:SetSize(80, 21)
    toggleButton:SetText("Socialize")
    self:togglePanel(self.mainFrame, SocializeStorage.getKey("showPanel"))
    toggleButton:SetScript("OnClick", function()
        if SocializeStorage.getKey("showPanel") then
            SocializeStorage.setKey("showPanel",  false)
        else
            SocializeStorage.setKey("showPanel",  true)
        end
        self:togglePanel(self.mainFrame, SocializeStorage.getKey("showPanel"))
    end)
    self.toggleButton = toggleButton
end

function SocializeFrame:initMainFrame(anchorTo)
    -- Create a Frame
    local mainFrame = CreateFrame("Frame", "Main frame", anchorTo)
    mainFrame:SetSize(350, 445)
    mainFrame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });

    mainFrame:SetPoint("TOPLEFT", anchorTo, "TOPRIGHT", -15, -12)
    mainFrame:SetFrameLevel(10)
    mainFrame:EnableMouse(true)
    mainFrame:SetMovable(true)
    mainFrame:SetClampedToScreen(true) -- This line ensures the frame won't be dragged off the screen
    mainFrame:RegisterForDrag("LeftButton") -- "LeftButton" makes the frame only draggable by holding down the left mouse button
    mainFrame:SetScript("OnDragStart", mainFrame.StartMoving)
    mainFrame:SetScript("OnDragStop", mainFrame.StopMovingOrSizing)

    self.mainFrame = mainFrame
end



function SocializeFrame:initAddFriendButton()
    local addFriendButton = CreateFrame("Button", nil, self.mainFrame, "GameMenuButtonTemplate")
    addFriendButton:SetPoint("BOTTOMLEFT", self.mainFrame, "BOTTOMLEFT", 2, 3)
    addFriendButton:SetText("Add friend")
    addFriendButton:SetSize(100,23)
    self.addFriendButton = addFriendButton
end

function SocializeFrame:activateAddFriendButtonListener()
    self.addFriendButton:SetScript("OnClick", function()
        SocializeFriendAddPopup.popup(self.friendsFrame)
    end)
end

function SocializeFrame:initSetStatusButton()
    local setStatusButton = CreateFrame("Button", nil, self.mainFrame, "GameMenuButtonTemplate")
    setStatusButton:SetPoint("TOPLEFT", self.mainFrame, "TOPLEFT", 2, -3)
    setStatusButton:SetText("Set status")
    setStatusButton:SetSize(80,20)
    self.setStatusButton = setStatusButton
end

function SocializeFrame:initSettingsButton()
    local button = CreateFrame("Button", nil, self.mainFrame, "GameMenuButtonTemplate")
    button:SetSize(80, 20)
    button:SetPoint("TOPRIGHT", self.mainFrame, "TOPRIGHT", -2, -3)
    button:SetText("Settings")
    self.settingsButton = button
end

function SocializeFrame:initAboutButton()
    local button = CreateFrame("Button", nil, self.mainFrame, "GameMenuButtonTemplate")
    button:SetSize(80, 20)
    button:SetPoint("TOPRIGHT", self.mainFrame, "TOPRIGHT", -80, -3)
    button:SetText("About")
    self.aboutButton = button
end

function SocializeFrame:activateSetStatusButtonListener()
    self.setStatusButton:SetScript("OnClick", function()
        SocializeStatusUpdatePopup.popup(self.statusTextElement)
    end)
end

function SocializeFrame:activateSettingsButtonListener()
    self.settingsButton:SetScript("OnClick", function()
        if self.settingsFrame.mainFrame:IsVisible() then
            self.settingsFrame.mainFrame:Hide()
        else
            self.settingsFrame.mainFrame:Show()
        end
    end)
end

function SocializeFrame:activateAboutButtonListener()
    self.aboutButton:SetScript("OnClick", function()
        SocializeAboutPopup.popup()
    end)
end

function SocializeFrame:initStatusTextElement()
    local statusString = self.mainFrame:CreateFontString("StatusText", "OVERLAY")
    statusString:SetPoint("TOPLEFT", self.mainFrame, "TOPLEFT", 5, -25) -- This positions the text in the center of the frame
    local location_font, _, location_font_style = statusString:GetFont()
    statusString:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style)

    local player = SocializeStorage.getPlayer()
    statusString:SetText("Status: "..player.status)
    self.statusTextElement = statusString
end

function SocializeFrame.addSlash(createFrom, parent, x, y)
    local slash = createFrom:CreateFontString("StatusText", "OVERLAY")
    slash:SetPoint("TOPRIGHT", parent, "TOPRIGHT", x, y) -- This positions the text in the center of the frame
    local location_font, _, location_font_style = slash:GetFont()
    slash:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style)
    slash:SetText("/")
    slash:SetTextColor(1, 1, 1, 1)
end

function SocializeFrame:initOnlineStatusDropdown()
    local dropDown = CreateFrame("Button", "MyDropDownMenu", self.mainFrame, "UIDropDownMenuTemplate")

    -- Position the dropdown menu
    dropDown:SetPoint("TOPRIGHT", self.mainFrame, "TOPRIGHT", 10, -43)

    local function updateMenu()
        -- Clear existing menu
        UIDropDownMenu_Initialize(dropDown, function() end)

        -- Recreate the menu using the updated Socialize["showOnline"] value
        UIDropDownMenu_Initialize(dropDown, function(self, level, menuList)
            local info = UIDropDownMenu_CreateInfo()
            info.text = "Online"
            info.func = function()
                Socialize["showOnline"] = true
                SocializeCommunication.pingFriends(3, 6)
                UIDropDownMenu_SetSelectedID(dropDown, 1)
                updateMenu()
            end
            info.checked = Socialize["showOnline"] == true
            UIDropDownMenu_AddButton(info)

            info.text = "Offline"
            info.func = function()
                Socialize["showOnline"] = false
                UIDropDownMenu_SetSelectedID(dropDown, 2)
                updateMenu()
                SocializeStorage.markAllFriendsAsOffline()
            end
            info.checked = Socialize["showOnline"] == false
            UIDropDownMenu_AddButton(info)
        end)
    end

    -- Initialize the select drop down for the first time
    updateMenu()
    local selectedId = 2
    if Socialize["showOnline"] then
        selectedId = 1
    end
    UIDropDownMenu_SetSelectedID(dropDown, selectedId)
    -- Set the width of the dropdown menu
    UIDropDownMenu_SetWidth(dropDown, 70)
end

function SocializeFrame.setOnlineStatus(status)
    Socialize["showOnline"] = status
end

function SocializeFrame:initFriendMetaTextElements()

    local metaTextElement = self.mainFrame:CreateFontString("StatusText", "OVERLAY")
    metaTextElement:SetPoint("BOTTOMRIGHT", self.mainFrame, "BOTTOMRIGHT", -10, 8) -- This positions the text in the center of the frame
    local _, _, location_font_style = metaTextElement:GetFont()
    metaTextElement:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style)
    metaTextElement:SetTextColor(1, 1, 1, 1)
    self.metaTextElement = metaTextElement

    local label = self.mainFrame:CreateFontString("StatusText", "OVERLAY")
    label:SetPoint("RIGHT", metaTextElement, "LEFT", -5, 0) -- This positions the text in the center of the frame
    local _, _, location_font_style = label:GetFont()
    label:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style)
    label:SetText("Friends: ")
    label:SetTextColor(1, 1, 1, 1)
end

function SocializeFrame:initSearchBox()
    local inputField = CreateFrame("EditBox", "MyEditBoxName", self.mainFrame, "InputBoxTemplate")
    inputField:SetPoint("TOPLEFT", self.mainFrame, "TOPLEFT", 10, -43)
    inputField:SetAutoFocus(false)
    inputField:SetWidth(100)
    inputField:SetHeight(30)
    self.searchBox = inputField
end

function SocializeFrame:activateOnTextChangeEventListener()
    self.searchBox:SetScript("OnTextChanged", function(searchBoxSelf, userInput)
        if userInput then
            local text = searchBoxSelf:GetText()
            self.friendsFrame.searchValue = text
        end
    end)
    self.searchBox:SetScript("OnEnterPressed", function(searchBoxSelf)
        searchBoxSelf:ClearFocus()
    end)
end

function SocializeFrame:activateOnShowEventListener()
    self.mainFrame:SetScript("OnShow", function()
        --SocializeFriendsFrame.getInstance():updateComponent()
        SocializeCommunication.pingFriends(3, 6)
    end)
end

function SocializeFrame:updateComponent()
    self.mainFrame:SetAlpha(SocializeConfig.default.advanced.alpha)
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.socializeFrame.backdropColor));
    self.mainFrame:SetBackdropBorderColor(unpack(SocializeConfig.default.colors.socializeFrame.borderColor));
    local player = SocializeStorage.getPlayer()
    self.statusTextElement:SetText("Status: "..player.status)
    self.statusTextElement:SetTextColor(unpack(SocializeConfig.default.colors.socializeFrame.statusTextColor))
    self.friendsFrame:updateComponent()
    self.friendInfoFrame:updateComponent()
    self.settingsFrame:updateComponent()
    local onlineFriends = self.friendsFrame.friendsMeta[3] or 0
    self.metaTextElement:SetText(onlineFriends.."/"..self.friendsFrame.friendsMeta.total)
end
