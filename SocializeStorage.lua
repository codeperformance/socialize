SocializeStorage = {

}

function SocializeStorage.initialize(playerName)
    Socialize = Socialize or {}
    Socialize["showPanel"] = Socialize["showPanel"] or false
    Socialize["showOnline"] = Socialize["showOnline"] or {}
    Socialize["characters"] = Socialize["characters"] or {}
    --This is for backward-compatibility as a character used to be a simple string, but now is an elaborate object
    if not Socialize["characters"][playerName] or type(Socialize["characters"][playerName]) == "string" then
        Socialize["characters"][playerName] = {}
        Socialize["characters"][playerName]["name"] = playerName
    end
    --End of backward-compatibility
    Socialize["characters"][playerName]["status"] = Socialize["characters"][playerName]["status"] or ""
    Socialize["friends"] = Socialize["friends"] or {}
    SocializeStorage.markAllFriendsAsOffline()
    return Socialize["showPanel"], Socialize["showOnline"]
end

function SocializeStorage.markAllFriendsAsOffline()
    for _, friend in pairs(Socialize["friends"]) do
        friend.online = false
        friend.hasBeenOnline = false
    end
end

function SocializeStorage.addFriend(friendName, ID, CustomStatus)
    if Socialize["friends"][friendName] then
        if ID then
            Socialize["friends"][friendName].hasManualID = true
            Socialize["friends"][friendName].ID = ID
            Socialize["friends"][friendName].status = CustomStatus or Socialize["friends"][friendName].status
        end
        return Socialize["friends"][friendName]
    end
    Socialize["friends"][friendName] = SocializeFriendWrapper.newFriendTable(friendName, ID, CustomStatus)
    return Socialize["friends"][friendName]
end

---@return Friend
function SocializeStorage.getFriend(friendName)
    return Socialize["friends"][friendName]
end

---@return Friend[]
function SocializeStorage.findFriendsByID(ID)
    local friendsWithID = {}
    for friendName, friend in pairs(SocializeStorage.getFriends()) do
        if friend.ID == ID then
            friendsWithID[friendName] = friend
        end
    end
    return friendsWithID
end

---@return SocializeFriendWrapper
function SocializeStorage.getFriendWrapper(friendName)
    return SocializeFriendWrapper:new(Socialize["friends"][friendName])
end

---@return Friend[]
function SocializeStorage.getFriends()
    return Socialize["friends"]
end

function SocializeStorage.removeFriend(friendName)
    Socialize["friends"][friendName] = nil
end
--TODO: Move to SocializeFriendWrapper
---@param friend Friend
function SocializeStorage.ignoreFriend(friend, boolean)
    friend.ignored = boolean
    if friend.ignored then
        friend.online = false
        friend.status = "IGNORED"
        friend.location = ""
        --friend.verifiedAs = {} -- dont destroy the relationship to prevent complications
        friend.metadata = {}
        SocializeFriendWrapper.initializeEquipment(friend)
        SocializeFriendWrapper.initializeProgress(friend)
    else
        friend.status = ""
        friend.hasBeenOnline = true
    end
end

function SocializeStorage:createDataPackage()
    local player = Socialize["characters"][SocializeSession.playerName]
    return player.location .. "||" .. player.status .. "||"
end

function SocializeStorage.setKey(key, value)
    Socialize[key] = value
end

function SocializeStorage.getKey(key)
    return Socialize[key]
end
function SocializeStorage.getPlayer()
    return Socialize["characters"][SocializeSession.playerName]
end

function SocializeStorage.updateLocationString()
    local HighRiskAura = 1004019
    local NoRiskAura = 1004119
    local PVEModeAura = 9931032
    local inInstance = IsInInstance()

    local locationString = ""
    local instanceName, instanceType, difficulty, difficultyName,
    maxPlayers, playerDifficulty, isDynamicInstance = GetInstanceInfo()
    local difficulties = {
        "Normal",
        "Heroic",
        "Mythic",
        "Ascended"
    }
    local zoneText = GetZoneText()
    local minimapZoneText = GetMinimapZoneText()
    local isInPVE = SocializeGlobalHelper.hasAura(PVEModeAura) or (inInstance and instanceType ~= "pvp")
    if instanceType == "none" or instanceType == "pvp"  then

        if zoneText == minimapZoneText or not isInPVE then
            locationString = zoneText
        else
            locationString = zoneText .. " - " .. minimapZoneText
        end
    else
        local difficultyString = difficulties[difficulty] .. " "
        if difficulty == 3 and maxPlayers == 5 then
            -- is in mythic dungeon OR manastorm
            if C_Manastorm.IsInManastorm() then
                difficultyString = "Manastorm " .. C_Manastorm.GetActiveLevel() .. " "
            else
                difficultyString = "M+" .. C_MythicPlus.GetActiveKeystoneInfo().keystoneLevel .. " "
            end
        end
        if zoneText == minimapZoneText or not isInPVE then
            locationString = difficultyString .. zoneText
        else
            locationString = difficultyString .. zoneText .. " - " .. minimapZoneText
        end
    end
    local player = SocializeStorage.getPlayer()
    player.location = locationString
end

---@param friend Friend
function SocializeStorage.updateFriendMetadata(friend, parameters)
    if #parameters > 0 then
        friend.location = parameters[1]
    end
    if #parameters > 1 then
        friend.status = parameters[2]
    else
        friend.status = ""
    end
end

function SocializeStorage.setOnlineStatus(status)
    Socialize["showOnline"] = status
end
