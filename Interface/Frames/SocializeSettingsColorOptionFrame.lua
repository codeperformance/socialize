---@class SocializeSettingsColorOptionFrame
SocializeSettingsColorOptionFrame = {
    mainFrame = {},
    colorPickerButton = {},
    colorPickerLabel = {},
    data = {},
    label = "",
}

function SocializeSettingsColorOptionFrame:new(label, data, parent)
    ---@type SocializeSettingsColorOptionFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeSettingsColorOptionFrame.__index = self
    instance.data = data
    instance.label = label
    instance.mainFrame = SocializeSettingsColorOptionFrame.initMainFrame(parent)
    instance.colorPickerButton = SocializeSettingsColorOptionFrame.initButton(data, instance.mainFrame)
    instance.colorPickerLabel = SocializeSettingsColorOptionFrame.initLabel(label, instance.colorPickerButton)
    return instance
end

function SocializeSettingsColorOptionFrame.initMainFrame(parent)
    local mainFrame = CreateFrame("Frame", "Main frame",  parent)
    mainFrame:SetSize(350, 24)
    mainFrame:SetPoint("TOPLEFT", parent, "BOTTOMLEFT", 0, 0)
    return mainFrame

end

function SocializeSettingsColorOptionFrame.initButton(data, parent)
    local colorPickerButton = CreateFrame("Button", nil, parent)
    colorPickerButton:SetSize(24, 24)
    colorPickerButton:SetPoint("TOPLEFT", parent, "TOPLEFT", 0, 0)
    colorPickerButton:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 12,
        insets = { left = 5, right = 5, top = 5, bottom = 5 }
    });
    colorPickerButton:SetBackdropColor(data[1], data[2], data[3])
    colorPickerButton:SetBackdropBorderColor(1, 1, 1)
    colorPickerButton:SetScript("OnClick", function()
        local currentColor = {}
        currentColor[1] = data[1]
        currentColor[2] = data[2]
        currentColor[3] = data[3]
        currentColor[4] = data[4]
        ColorPickerFrame.func = function() -- The function that will be called when the user changes the color
            local red, green, blue = ColorPickerFrame:GetColorRGB()
            data[1] = red
            data[2] = green
            data[3] = blue
            colorPickerButton:SetBackdropColor(data[1], data[2], data[3])
        end

        ColorPickerFrame.cancelFunc = function()
            data[1], data[2], data[3], data[4] = unpack(currentColor)
            colorPickerButton:SetBackdropColor(data[1], data[2], data[3])
        end
        --ColorPickerFrame is a singleton.
        --Calling SetColorRGB too early would cause the .func to trigger and mess up previous data
        ColorPickerFrame:SetColorRGB(data[1], data[2], data[3])
        ColorPickerFrame:Show()
    end)
    return colorPickerButton
end

function SocializeSettingsColorOptionFrame.initLabel(label, parent)
    local colorPickerLabel = parent:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    colorPickerLabel:SetPoint("LEFT", parent, "RIGHT", 5, 0)
    colorPickerLabel:SetText(label)
    return colorPickerLabel
end

function SocializeSettingsColorOptionFrame:updateComponent()
    self.colorPickerButton:SetBackdropColor(self.data[1], self.data[2], self.data[3])
    self.colorPickerButton:SetBackdropBorderColor(1, 1, 1)
end