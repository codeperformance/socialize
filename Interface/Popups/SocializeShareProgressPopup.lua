SocializeShareProgressPopup = {}

function SocializeShareProgressPopup.popup(friendName)
    StaticPopupDialogs["SOCIALIZE_SHARE_PROGRESS"] = {
        text = "Are you sure you want to share your current progress with "..friendName.."?",
        button1 = "Jow",
        button2 = "Uhh no",
        hasEditBox = false,
        OnAccept = function(self)
            SocializeCommunicationInternal.sendAllProgress(friendName)
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
    }

    StaticPopup_Show("SOCIALIZE_SHARE_PROGRESS")
end

function SocializeShareProgressPopup.collectAndSendProgress(friendName, progressType)
    for progressKey, difficulty in pairs(SocializeProgress.getProgress(progressType)) do
        SocializeCommunicationInternal.sendProgress(friendName, progressType, progressKey, difficulty)
    end
end
