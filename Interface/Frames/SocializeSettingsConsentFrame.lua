---@class SocializeSettingsConsentFrame
SocializeSettingsConsentFrame = {
    mainFrame = {},
    scrollPanelWithAnchorCollection = {},
    childFrame = {},
    resetButton = {},
    checkboxes = {},
}

function SocializeSettingsConsentFrame:new(parent, bottomRightParent)
    ---@type SocializeSettingsConsentFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeSettingsConsentFrame.__index = self
    instance.mainFrame = SocializeFrameGenerator.mainFrameWithScrollPanel(parent, bottomRightParent)
    instance.mainFrame.scrollbar:SetMinMaxValues(1, 1)--hardcoded to 1 as no scrolling is needed for now
    --instance.resetButton = SocializeSettingsColorFrame.initResetButton(instance.mainFrame)
    local checkboxPairs = SocializeSettingsConsentFrame.getCheckboxData()
    instance.checkboxes = SocializeSettingsConsentFrame.initCheckboxes(checkboxPairs, instance.mainFrame.anchor)
    return instance
end

function SocializeSettingsConsentFrame.getCheckboxData()
    return {
        {
            label = "Auto add friend's alts",
            data = SocializeConfig.default.consent,
            dataKey = "autoAcceptAltFriendRequests",
            tooltipDescription = "Automatically accept when a friend logs into an alt and wants to add it to your friend list"
        },
        {
            label = "Auto send alts to friends",
            data = SocializeConfig.default.consent,
            dataKey = "autoSendFriendRequestFromAlts",
            tooltipDescription = "Automatically add existing friends when you log into an alt"
        },
        {
            label = "Auto send progress updates to friends",
            data = SocializeConfig.default.consent,
            dataKey = "autoSendProgress",
            tooltipDescription = "Automatically send your progress to friends once one of the tracked achievements is achieved"
        },
        --TODO: Decide if feature should be visible or hidden
        --{
        --    label = "Auto accept friend requests",
        --    data = SocializeConfig.default.consent,
        --    dataKey = "autoAcceptFriendRequests",
        --    tooltipDescription = "Automatically accept incoming friend requests. You will not get a popup, but will see a message in the chat."
        --},
    }
end

function SocializeSettingsConsentFrame.initCheckboxes(checkboxPairs, startParent)
    local checkboxes = {}
    local lastParent = startParent
    for _, checkboxData in ipairs(checkboxPairs) do
        local checkbox = SocializeFrameGenerator.initCheckbox(checkboxData.label, lastParent, checkboxData.data, checkboxData.dataKey, checkboxData.tooltipDescription)
        lastParent = checkbox
        checkboxes[checkboxData.label] = checkbox
    end
    return checkboxes
end

function SocializeSettingsConsentFrame:updateComponent()
    local checkboxData = SocializeSettingsConsentFrame.getCheckboxData()
    for _, checkbox in ipairs(checkboxData) do
        self.checkboxes[checkbox.label]:SetChecked(checkbox.data[checkbox.dataKey])
    end
end