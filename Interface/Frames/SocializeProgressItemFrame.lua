---@class SocializeProgressItemFrame
SocializeProgressItemFrame = {
    progressType = nil,
    mainFrame = nil,
    referenceFrame = nil,
    progressFrame = nil,
    titleText = nil,
    labelElements = {},
    progressElements = {},
    progressKeys = nil,
}

function SocializeProgressItemFrame:new(title, progressType, progressFrame, width, height, x, y)
    ---@type SocializeProgressItemFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeProgressItemFrame.__index = self
    instance.progressType = progressType
    instance.progressFrame = progressFrame
    instance.progressElements = {}
    instance.labelElements = {}
    instance.progressKeys = {}
    instance:initMainFrame(progressFrame.mainFrame.childFrame, width, height, x, y)
    instance:initReferenceFrame(progressFrame.mainFrame.childFrame)
    instance:initSlotFrames()
    instance:initTitleText(title)
    return instance
end

function SocializeProgressItemFrame:updateComponent()
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.backdropColor));
    self.mainFrame:SetBackdropBorderColor(unpack(SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.borderColor));
    self.titleText:SetTextColor(unpack(SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.titleTextColor))
    local friend = SocializeFriendsFrame.getInstance():getSelectedFriend()
    if not friend then
        return
    end
    local progress = friend:getProgress(self.progressType)
    for i, progressKey in ipairs(self.progressKeys) do
        self.labelElements[progressKey]:SetTextColor(unpack(SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.labelTextColor))
        self.progressElements[progressKey]:SetText(progress[progressKey] or "Missing")
        self.progressElements[progressKey]:SetTextColor(unpack(SocializeConfig.default.colors.friendInfoFrame.progress.progressItem[progress[progressKey] or "Missing"]))
    end
end

function SocializeProgressItemFrame:initMainFrame(parent, width, height, x, y)
    local frame = CreateFrame("Frame", nil, parent)
    frame:SetPoint("TOPLEFT", parent, "TOPLEFT", x, y)
    frame:SetSize(width, height)
    --frame:SetAllPoints(true)
    frame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });
    self.mainFrame = frame
end

function SocializeProgressItemFrame:initReferenceFrame(parent)
    local referenceFrame = CreateFrame("Frame", nil, parent)
    referenceFrame:SetPoint("TOPLEFT", self.mainFrame, "TOPLEFT", 8, -20)
    referenceFrame:SetSize(parent:GetWidth()/2-18,1)
    self.referenceFrame = referenceFrame
end

function SocializeProgressItemFrame:initTitleText(title)
    local titleText = self.mainFrame:CreateFontString("StatusText", "OVERLAY")
    titleText:SetPoint("TOP", self.mainFrame, "TOP", 0, -5)
    local _, _, location_font_style = titleText:GetFont()
    titleText:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style)
    titleText:SetText(title)
    self.titleText = titleText
end

function SocializeProgressItemFrame:initSlotFrames()
    --sorting logic
    local progressMapping = SocializeProgress.getMapping(self.progressType)
    local keys = {}
    for key in pairs(progressMapping) do
        table.insert(keys, key)
    end
    table.sort(keys, function(a, b) return progressMapping[a].OrderID < progressMapping[b].OrderID end)
    for _, progressKey in ipairs(keys) do
        table.insert(self.progressKeys, progressKey)
    end
    --/sorting logic
    local referenceFrameLeft = self.referenceFrame
    local referenceFrameRight = self.referenceFrame
    for i, progressKey in  ipairs(self.progressKeys) do
        local slotText = self.mainFrame:CreateFontString("StatusText", "OVERLAY")
        slotText:SetPoint("TOPLEFT", referenceFrameLeft, "BOTTOMLEFT", 0, 0)
        local _, _, location_font_style = slotText:GetFont()
        slotText:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style)
        slotText:SetText(progressKey:gsub("^%l", string.upper))--ucfirst
        local progressTextElement = self.mainFrame:CreateFontString("StatusText", "OVERLAY")
        progressTextElement:SetPoint("TOPRIGHT", referenceFrameRight, "BOTTOMRIGHT", 0, 0)
        local _, _, location_font_style2 = progressTextElement:GetFont()
        progressTextElement:SetFont("Fonts\\FRIZQT__.ttf", 11, location_font_style2)
        referenceFrameLeft = slotText
        self.labelElements[progressKey] = slotText
        self.progressElements[progressKey] = progressTextElement
        referenceFrameRight = progressTextElement
    end
end
