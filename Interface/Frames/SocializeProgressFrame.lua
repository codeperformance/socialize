---@class SocializeProgressFrame
SocializeProgressFrame = {
    friendInfoFrame = nil,
    mainFrame = nil,
    tier6ProgressFrame = nil,
    tier5ProgressFrame = nil,
    blackTempleProgressFrame = nil,
    mountHyjalProgressFrame = nil,
    ---@type SocializeProgressItemFrame[]
    progressItemFrames = {},
}

function SocializeProgressFrame:new(parent, anchorPoint)
    ---@type SocializeProgressFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeProgressFrame.__index = self
    instance.friendInfoFrame = parent
    --instance:initMainFrame(parent.mainFrame, anchorPoint)
    instance.mainFrame = SocializeFrameGenerator.mainFrameWithScrollPanel(anchorPoint, parent.mainFrame)
    instance:initItemFrames()
    return instance
end

function SocializeProgressFrame:updateComponent()
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendInfoFrame.progress.backdropColor))
    self.mainFrame:SetBackdropBorderColor(unpack(SocializeConfig.default.colors.friendInfoFrame.progress.borderColor))
    for key, frame in pairs(self.progressItemFrames) do
        frame:updateComponent()
    end
end

function SocializeProgressFrame:initMainFrame(parent, anchorPoint)
    local frame = CreateFrame("Frame", nil, parent)
    frame:SetPoint("TOPLEFT", anchorPoint, "BOTTOMLEFT")
    frame:SetPoint("BOTTOMRIGHT", parent, "BOTTOMRIGHT")
    --tabFrames[i]:SetAllPoints(true)
    frame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });

    self.mainFrame = frame
end

function SocializeProgressFrame:addItemFrame(title, type, width, height, x, y)
    table.insert(self.progressItemFrames, SocializeProgressItemFrame:new(title, type, self, width, height, x, y))
end

function SocializeProgressFrame:initItemFrames()
    local leftMargin = 3
    local width = self.mainFrame:GetWidth()/2 - 15
    local y = 0
    local height = 120
    self:addItemFrame(
            "T6 Progress",
            SocializeProgressType.tier6,
            width,
            height,
            -leftMargin+0,
            y
    )
    self:addItemFrame(
            "T5 Progress",
            SocializeProgressType.tier5,
            width,
            height,
            -leftMargin+width,
            y
    )
    y = y - height
    height = 130
    self:addItemFrame(
            "Black Temple Progress",
            SocializeProgressType.blackTemple,
            width,
            height,
            -leftMargin+0,
            y
    )
    self:addItemFrame(
            "Mount Hyjal Progress",
            SocializeProgressType.mountHyjal,
            width,
            height,
            -leftMargin+width,
            y
    )

    y = y - height
    height = 110
    self:addItemFrame(
            "Sunwell Progress",
            SocializeProgressType.sunwellPlateau,
            width,
            height,
            -leftMargin+0,
            y
    )
    self:addItemFrame(
            "Zul'aman Progress",
            SocializeProgressType.zulAman,
            width,
            height,
            -leftMargin+width,
            y
    )
    y = y - height
    height = 50
    self:addItemFrame(
            "Gruul's lair Progress",
            SocializeProgressType.gruulsLair,
            width,
            height,
            -leftMargin+0,
            y
    )
    self:addItemFrame(
            "Magtheridon Progress",
            SocializeProgressType.magtheridonsLair,
            width,
            height,
            -leftMargin+width,
            y
    )
    y = y - height
    height = 130
    self:addItemFrame(
            "Serpentshrine Progress",
            SocializeProgressType.serpentShrineCavern,
            width,
            height,
            -leftMargin+0,
            y
    )
    self:addItemFrame(
            "Karazhan Progress",
            SocializeProgressType.karazhan,
            width,
            height,
            -leftMargin+width,
            y
    )
    y = y - height
    height = 80
    self:addItemFrame(
            "Tempest Keep Progress",
            SocializeProgressType.tempestKeep,
            width,
            height,
            -leftMargin+0,
            y
    )
    --self:addItemFrame(
    --        "Karazhan Progress",
    --        SocializeProgressType.karazhan,
    --        width,
    --        120,
    --        -leftMargin+width,
    --        y
    --)
end





