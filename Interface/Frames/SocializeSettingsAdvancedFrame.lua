---@class SocializeSettingsAdvancedFrame
SocializeSettingsAdvancedFrame = {
    mainFrame = {},
    mainFrame = {},
    childFrame = {},
    resetButton = {},
    inputs = {},
}

function SocializeSettingsAdvancedFrame:new(parent, bottomRightParent)
    ---@type SocializeSettingsAdvancedFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeSettingsAdvancedFrame.__index = self
    instance.mainFrame = SocializeFrameGenerator.mainFrameWithScrollPanel(parent, bottomRightParent)
    instance.mainFrame.scrollbar:SetMinMaxValues(1, 1)--hardcoded to 1 as no scrolling is needed for now
    --instance.resetButton = SocializeSettingsColorFrame.initResetButton(instance.mainFrame)
    local inputData = SocializeSettingsAdvancedFrame.getInputData()
    instance.mainFrame.anchor:SetPoint("TOPLEFT", instance.mainFrame.childFrame, "TOPLEFT", 5, 0)
    instance.inputs = SocializeSettingsAdvancedFrame.initInputs(inputData, instance.mainFrame.anchor)
    return instance
end

function SocializeSettingsAdvancedFrame.initNumberInputWithLabel(label, parent, data, dataKey, min, max, step)
    local frame = CreateFrame("Frame", nil, parent, "BackdropTemplate")
    frame:SetSize(parent:GetWidth(), 40)
    frame:SetPoint("TOPLEFT", parent, "BOTTOMLEFT")

    -- Creating the slider
    local slider = CreateFrame("Slider", "slider"..label, frame, "OptionsSliderTemplate")
    slider:SetWidth(frame:GetWidth())
    slider:SetHeight(16)
    slider:SetPoint("CENTER")
    slider:SetOrientation('HORIZONTAL')
    slider:SetMinMaxValues(min, max) -- Min and max values of the slider
    -- Step
    slider.tooltipText = label -- Tooltip on mouseover

    -- Get the slider text (min, value, max) handles.
    local sliderText = getglobal(slider:GetName() .. 'Text')
    local sliderLow = getglobal(slider:GetName() .. 'Low')
    local sliderHigh = getglobal(slider:GetName() .. 'High')

    -- Set the handles.
    sliderLow:SetText(min)
    sliderHigh:SetText(max)
    sliderText:SetText(data[dataKey])

    -- Create a function to update the text value when the slider is moved
    slider:SetScript("OnValueChanged", function(self,value)
        local mult = 10^(2)
        local rounded = math.floor(value * mult + 0.5) / mult

        data[dataKey] = value
        --self:SetValue(rounded) --TODO: investigate if this is needed as this currently breaks fstack
        sliderText:SetText(rounded) -- Update the text display
    end)
    slider:SetValue(data[dataKey]) -- Initial value
    slider:SetValueStep(step)
    frame.slider = slider
    return frame
end


function SocializeSettingsAdvancedFrame.getInputData()
    return {
        {
            label = "When a friend reloads, give them x seconds extra before they are marked offline",
            data = SocializeConfig.default.advanced,
            dataKey = "extraSecondsToReload",
            min = 5,
            max = 60,
            step = 1,
        },
        {
            label = "Ping interval(seconds)",
            data = SocializeConfig.default.advanced,
            dataKey = "pingIntervalInSeconds",
            min = 1,
            max = 30,
            step = 1,
        },
        {
            label = "UI Refresh interval(seconds)",
            data = SocializeConfig.default.advanced,
            dataKey = "UIRefreshIntervalInSeconds",
            min = 0.01,
            max = 2,
            step = 0.01,
        },
        {
            label = "When friend is offline, do a final check after x seconds",
            data = SocializeConfig.default.advanced,
            dataKey = "lastCheckAfterSeconds",
            min = 5,
            max = 120,
            step = 1,
        },
        {
            label = "Alpha",
            data = SocializeConfig.default.advanced,
            dataKey = "alpha",
            min = 0.3,
            max = 1,
            step = 0.01,
        },
    }
end

function SocializeSettingsAdvancedFrame.initInputs(inputData, startParent)
    local inputs = {}
    local lastParent = startParent
    for _, inputObject in ipairs(inputData) do
        local input = SocializeSettingsAdvancedFrame.initNumberInputWithLabel(inputObject.label, lastParent, inputObject.data, inputObject.dataKey, inputObject.min, inputObject.max, inputObject.step)
        lastParent = input
        inputs[inputObject.label] = input
    end
    return inputs
end

function SocializeSettingsAdvancedFrame:updateComponent()
    local inputData = SocializeSettingsAdvancedFrame.getInputData()
    for _, input in ipairs(inputData) do
        self.inputs[input.label].slider:SetValue(input.data[input.dataKey])
    end
end