SocializeCommunicationEventHandler = {}

function SocializeCommunicationEventHandler.onFriendRequest(sender)
    SocializeFriendConfirmationPopup.popup(sender, SocializeFriendsFrame.getInstance())
end

---@param friend Friend
function SocializeCommunicationEventHandler.onAreYouOnline(friend, message)
    if not Socialize.showOnline then
        return
    end
    local splitMessage = SocializeString.split(message, "\1")
    friend.lastResponse = time()
    if not friend.online then
        local isSocializeFrameVisible = SocializeFrame.instance.mainFrame:IsVisible()
        if not isSocializeFrameVisible then
            SocializePrint.systemMessage(friend.name .. " has come online")
        end
        friend.online = true
        friend.hasBeenOnline = true
    end
    if #splitMessage > 0 then
        local friendWrapper = SocializeFriendWrapper:new(friend)
        local foreignKey = splitMessage[3] or splitMessage[1]--either fields can contain the key
        friendWrapper:updateVerifiedAsForeignKey(foreignKey)
    end
    SocializeCommunicationInternal.sendIAmOnline(friend.name)
end

--- Check if you want to add someone based on the fact you might know their main.
---
--- Returns true if target is acquaintance, false if not
function SocializeCommunicationEventHandler.onAreYouOnlineAndAcquaintance(sender, message)
    local splitMessage = SocializeString.split(message, "\1")
    if not splitMessage[1] or not splitMessage[2] then
        return false
    end
    local knownName = splitMessage[1]
    local knownSecret = splitMessage[2]
    --local friendSecret = splitMessage[3] --3rd key will be used in SocializeCommunicationEventHandler.onAreYouOnline
    local knownFriend = SocializeStorage.getFriend(knownName)
    if not knownFriend or knownFriend.ignored then
        return --sorry mate, i dont (wanna) know that other person
    end
    local verified = false
    local VerifiedByName = ""
    for verifiedBy, friendRelationship in pairs(knownFriend.verifiedAs) do
        if friendRelationship.localKey == knownSecret then
            verified = true
            VerifiedByName = verifiedBy
            break
        end
    end
    if not verified then
        return false--final fuck off, pretender
    end
    --man, i had a feeling i knew you from somewhere, welcome!
    local friend = SocializeStorage.addFriend(sender)
    friend.ID = knownFriend.ID
    friend.hasManualID = knownFriend.hasManualID
    local friendWrapper = SocializeFriendWrapper:new(friend)

    SocializeFriendsFrame.getInstance():addFriend(friend)
    friendWrapper:addVerifiedAs()
    SocializePrint.systemMessage("Added acquaintance " .. friend.name .. ", who is verified with " .. VerifiedByName .. " with ID: " .. friend.ID)
    SocializeCommunicationEventHandler.onAreYouOnline(friend, message) --bubble up
    return true
end

---@param friend Friend
function SocializeCommunicationEventHandler.onIAmOnline(friend, message)
    if not Socialize.showOnline then
        return
    end
    friend.lastResponse = time()
    if not friend.online then

        local isSocializeFrameVisible = SocializeFrame.instance.mainFrame:IsVisible()
        if not isSocializeFrameVisible then
            SocializePrint.systemMessage(friend.name .. " has come online")
        end
        friend.online = true
        friend.hasBeenOnline = true
    end
    local messageParameters = SocializeString.split(message, "||")
    SocializeStorage.updateFriendMetadata(friend, messageParameters)
end

---@param friend Friend
function SocializeCommunicationEventHandler.onIGoOffline(friend)
    if not Socialize.showOnline then
        return
    end
    friend.lastResponse = time()
    if friend.online then
        SocializePrint.systemMessage(friend.name .. " has gone offline")
        friend.online = false
        friend.hasBeenOnline = false
    end
end

---@param friend Friend
function SocializeCommunicationEventHandler.onIAmReloading(friend, extraSecondsToReload)
    if not Socialize.showOnline then
        return
    end
    if not SocializeSession.isSelf(friend.name) then
        friend.lastResponse = time() + extraSecondsToReload
        friend.status = "RELOADING"
    end
end

---@param friend Friend
function SocializeCommunicationEventHandler.onShareEquipment(friend, message)
    if not friend.metadata then
        friend.metadata = {}
    end
    if not friend.metadata.equipment then
        friend.metadata.equipment = {}
    end
    local splitParameters = SocializeString.split(message, "\1")
    local slot = splitParameters[1]
    local item = splitParameters[2]
    if slot == "head" then
        --Since equipment is shared in bulk, and we only want to show message once
        SocializePrint.systemMessage(friend.name .. " shared equipment with you")
    end
    friend.metadata.equipment[slot] = item
end

---@param friend Friend
function SocializeCommunicationEventHandler.onShareProgress(friend, message)
    if not friend.metadata then
        friend.metadata = {}
    end
    if not friend.metadata.progress then
        friend.metadata.progress = {}
    end
    local splitParameters = SocializeString.split(message, "\1")
    local instance = splitParameters[1]
    local boss = splitParameters[2]
    local difficulty = splitParameters[3]
    if instance == "blackTemple" and boss == "najentus" then
        --Since progress is shared in bulk, and we only want to show message once
        SocializePrint.systemMessage(friend.name .. " shared progress with you")
    end
    if not friend.metadata.progress[instance] then
        friend.metadata.progress[instance] = {}
    end
    friend.metadata.progress[instance][boss] = difficulty
end