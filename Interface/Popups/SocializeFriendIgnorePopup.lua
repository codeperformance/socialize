SocializeFriendIgnorePopup = {}

function SocializeFriendIgnorePopup.popup(friend)
    local actionText = "IGNORE"
    if currentIgnoredStatus then
        actionText = "UN-IGNORE"
    end
    StaticPopupDialogs["SOCIALIZE_CONFIRM_IGNORE"] = {
        text = "Are you sure you want to "..actionText.." "..friend.name.."? This will be done account-wide and for all known alts!",
        button1 = "Yes",
        button2 = "No",
        OnAccept = function()
            local friendRelatives = SocializeStorage.findFriendsByID(friend.ID) --includes friend
            local setIgnoreTo = not friend.ignored
            for relativeName, relative in pairs(friendRelatives) do
                SocializeStorage.ignoreFriend(relative, setIgnoreTo)
            end

            --SocializeFriendsFrame.getInstance():updateComponent()
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        preferredIndex = 3,
    }
    StaticPopup_Show ("SOCIALIZE_CONFIRM_IGNORE")
end
