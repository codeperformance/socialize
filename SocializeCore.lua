---@class SocializeCore
SocializeCore = {
    socializeFrame = {},
    eventFrame = {},
    achievementFrame = {},
    isLoaded = false,
    communication = {},
    goingOffline = false,
}


function SocializeCore.enableDebugMessages()
    SocializeCommunicationInternal.debug = true
    SocializeCommunication.debug = true
end

function SocializeCore.disableDebugMessages()
    SocializeCommunicationInternal.debug = false
    SocializeCommunication.debug = false
end

function SocializeCore:new()
    ---@type SocializeCore
    local instance = {}
    setmetatable(instance, self)
    SocializeCore.__index = self
    instance:initEventFrame()
    instance:activateEventFrameListener()
    instance:initAchievementEventFrame()
    instance:activateAchievementEventFrame()
    return instance
end

function SocializeCore:initEventFrame()
    local eventFrame = CreateFrame("Frame", nil, UIParent)
    eventFrame:RegisterEvent("PLAYER_LOGIN")
    eventFrame:RegisterEvent("PLAYER_LOGOUT")
    self.eventFrame = eventFrame
end

function SocializeCore:activateEventFrameListener()
    self.eventFrame:SetScript("OnEvent", function(_, event, ...)
        if event == "PLAYER_LOGIN" then
            self:onLoginEvent()
        elseif event == "PLAYER_LOGOUT" then
            for friendName, friend in pairs(SocializeStorage.getFriends()) do
                SocializeCommunicationInternal.sendIAmReloading(friend.name)
            end
        end
    end)
end

function SocializeCore:initAchievementEventFrame()
    local achievementFrame = CreateFrame("Frame", nil, UIParent)
    achievementFrame:RegisterEvent("ACHIEVEMENT_EARNED")
    self.achievementFrame = achievementFrame
end

function SocializeCore:activateAchievementEventFrame()
    local achievementIds = SocializeProgress.getAchievementIds()
    self.achievementFrame:SetScript("OnEvent", function(_, event, id)
        if achievementIds[id] and SocializeConfig.default.consent.autoSendProgress and Socialize["showOnline"] then
            for _, friend in pairs(SocializeStorage.getFriends()) do
                if friend.online and not friend.ignored then
                    SocializeCommunicationInternal.sendAllProgress(friend.name)
                end
            end
        end
    end)
end

---Overriding the logout logic for both logout "CAMP" and exit game "QUIT" dialogs, plus the functions that spawn them
function SocializeCore:overrideLogoutLogic()
    StaticPopupDialogs["QUIT"].doCancel = true
    StaticPopupDialogs["QUIT"].OnAccept = function()
        StaticPopupDialogs["QUIT"].doCancel = false
        SocializePrint.systemMessage("Letting people know you are going offline")
        SocializeCommunication.sendGoingOffline()
        C_Timer.After(1, function()
            ForceQuit()
        end)
    end
    hooksecurefunc(StaticPopupDialogs["QUIT"], "OnHide", function()
        if StaticPopupDialogs["QUIT"].doCancel then
            SocializeCore.goingOffline = false
        end
    end)
    local QuitCopy= Quit
    Quit = function()
        SocializeCore.goingOffline = true
        QuitCopy()
        C_Timer.After(19, function()
            if SocializeCore.goingOffline then
                SocializeCommunication.sendGoingOffline()
            end
        end)
    end

    StaticPopupDialogs["CAMP"].doCancel = true
    hooksecurefunc(StaticPopupDialogs["CAMP"], "OnHide", function()
        if StaticPopupDialogs["CAMP"].doCancel then
            SocializeCore.goingOffline = false
        end
    end)
    local LogoutCopy= Logout
    Logout = function()
        SocializeCore.goingOffline = true
        LogoutCopy()
        C_Timer.After(19, function()
            if SocializeCore.goingOffline then
                SocializeCommunication.sendGoingOffline()
            end
        end)
    end
end

function SocializeCore:onLoginEvent()
    local frame_to_check = _G["FriendsFrame"]
    local selfName = SocializeSession.playerName
    if self.loaded or not frame_to_check then
        return
    end
    self.overrideLogoutLogic()
    self.loaded = true
    SocializeStorage.initialize(selfName)
    SocializeConfig.initialize()
    SocializeStorage.addFriend(selfName, "Me", "") --Ensure self is added to friends so they can see themselves
    local friends = SocializeStorage.getFriends()
    self.socializeFrame = SocializeFrame:new(FriendsListFrame)
    SocializeFriendsFrame.getInstance():spawnFriends(friends)
    self.communication = SocializeCommunication:new()

    SocializeCommunicationInternal.sendAllProgress(selfName) --Update self progress
    SocializeCommunicationInternal.sendAllEquipment(selfName) --Update self equipment

    --Start pinging
    SocializeCommunication.pingFriends(3, 6)
    self:runPingFriends()
    self:runUpdateUI()

    --TODO: Implement a way to quickly add many people without it being annoying
    --SocializeCore:testCustomLink()

end

function SocializeCore:testCustomLink()
    local function ShowCustomPopup()
        StaticPopupDialogs["MY_CUSTOM_POPUP"] = {
            text = "You clicked the link!",
            button1 = "OK",
            timeout = 0,
            whileDead = true,
            hideOnEscape = true,
        }
        StaticPopup_Show("MY_CUSTOM_POPUP")
    end
    local OriginalHyperlinkHandler = ChatFrame_OnHyperlinkShow
    -- Properly hook into hyperlink clicks
    local function HandleHyperlink(self, link, text, button)
        local linkType, data = strsplit(":", link)
        if linkType == "custommsg" then
            ShowCustomPopup()
            return
        end
        -- If it's not our custom link, pass it to the original handler
        OriginalHyperlinkHandler(self, link, text, button)
    end

    -- Hook into SetItemRef to recognize our custom hyperlink
    hooksecurefunc("SetItemRef", function(link, text, button, chatFrame)
        local linkType = strsplit(":", link)
        if linkType == "custommsg" then
            ShowCustomPopup()
        end
    end)

    -- Hook into chat frames to allow clickable links
    for i = 1, NUM_CHAT_WINDOWS do
        local chatFrame = _G["ChatFrame"..i]
        if chatFrame then
            chatFrame:SetScript("OnHyperlinkClick", HandleHyperlink)
        end
    end

    -- Function to send a clickable message in chat
    local function SendClickableMessage()
        local message = "Click |cff00ff00|Hcustommsg:1|h[this link]|h|r to see a popup!"
        DEFAULT_CHAT_FRAME:AddMessage(message)
    end

    -- Call this function to test the clickable message
    SendClickableMessage()

end

function SocializeCore:runPingFriends()
    local pingInterval = SocializeConfig.default.advanced.pingIntervalInSeconds
    local lastCheckAfterSeconds = SocializeConfig.default.advanced.lastCheckAfterSeconds
    C_Timer.After(pingInterval, function()
        if SocializeFrame.instance.mainFrame:IsVisible() then
            SocializeCommunication.pingOnlineFriends(pingInterval, lastCheckAfterSeconds)
        end
        self:runPingFriends()
    end)
end

function SocializeCore:runUpdateUI()
    C_Timer.After(SocializeConfig.default.advanced.UIRefreshIntervalInSeconds, function()
        self.socializeFrame:updateComponent()
        self:runUpdateUI()
    end)
end