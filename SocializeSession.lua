SocializeSession = {
    playerName = UnitName('player'),
}

function SocializeSession.isSelf(friendName)
    return friendName == SocializeSession.playerName
end