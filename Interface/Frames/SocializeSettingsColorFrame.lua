---@class SocializeSettingsColorFrame
SocializeSettingsColorFrame = {
    mainFrame = {},
    scrollFrame = {},
    childFrame = {},
    scrollbar = {},
    anchor = {},
    ---@type SocializeSettingsColorOptionFrame[]
    colorOptions = {},
    resetColorButton = {},
}

function SocializeSettingsColorFrame:new(parent, bottomRightParent)
    ---@type SocializeSettingsColorFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeSettingsColorFrame.__index = self
    instance.mainFrame = SocializeFrameGenerator.mainFrameWithScrollPanel(parent, bottomRightParent)
    --instance.resetColorButton = SocializeSettingsColorFrame.initResetButton(instance.mainFrame)
    instance.colorOptions = SocializeSettingsColorFrame.initColorOptions(instance.mainFrame.anchor)
    return instance
end

function SocializeSettingsColorFrame.initColorOptions(parent)
    local colorOptions = {
        { label = "Socialize Frame Backdrop Color", data = SocializeConfig.default.colors.socializeFrame.backdropColor },
        { label = "Socialize Frame Border Color", data = SocializeConfig.default.colors.socializeFrame.borderColor },
        { label = "Socialize Frame Status Text Color", data = SocializeConfig.default.colors.socializeFrame.statusTextColor },
        { label = "Friend Frame Offline Backdrop Color", data = SocializeConfig.default.colors.friendFrame.backdropColorOffline },
        { label = "Friend Frame Online Backdrop Color", data = SocializeConfig.default.colors.friendFrame.backdropColorOnline },
        { label = "Friend Frame Ignored Backdrop Color", data = SocializeConfig.default.colors.friendFrame.backdropColorIgnored },
        { label = "Friend Frame Highlighted Backdrop Color", data = SocializeConfig.default.colors.friendFrame.backdropColorHighlighted },
        { label = "Friend Frame Border Color", data = SocializeConfig.default.colors.friendFrame.borderColor },
        { label = "Friend Frame Self Border Color", data = SocializeConfig.default.colors.friendFrame.selfBorderColor },
        { label = "Friend Frame Name Text Color", data = SocializeConfig.default.colors.friendFrame.nameTextColor },
        { label = "Friend Frame Location Text Color", data = SocializeConfig.default.colors.friendFrame.locationTextColor },
        { label = "Friend Frame Status Text Normal Color", data = SocializeConfig.default.colors.friendFrame.statusTextColorNormal },
        { label = "Friend Frame Status Text Reloading Color", data = SocializeConfig.default.colors.friendFrame.statusTextColorReloading },
        { label = "Friend Info Frame Backdrop Color", data = SocializeConfig.default.colors.friendInfoFrame.backdropColor },
        { label = "Friend Info Frame Border Color", data = SocializeConfig.default.colors.friendInfoFrame.borderColor },
        { label = "Friend Info Frame Name Text Color", data = SocializeConfig.default.colors.friendInfoFrame.nameTextColor },
        { label = "Friend Info Frame Status Text Color", data = SocializeConfig.default.colors.friendInfoFrame.statusTextColor },
        { label = "Friend Info Frame Location Text Color", data = SocializeConfig.default.colors.friendInfoFrame.locationTextColor },
        { label = "EquipmentFrame Backdrop Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.backdropColor },
        { label = "EquipmentFrame Border Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.borderColor },
        { label = "EquipmentFrame Unavailable Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.unavailable },
        { label = "EquipmentFrame Uncommon Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.uncommon },
        { label = "EquipmentFrame Rare Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.rare },
        { label = "EquipmentFrame Epic Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.epic },
        { label = "EquipmentFrame Legendary Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.legendary },
        { label = "EquipmentFrame Vanity Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.vanity },
        { label = "EquipmentFrame Heirloom Color", data = SocializeConfig.default.colors.friendInfoFrame.equipment.heirloom },
        { label = "ProgressFrame Backdrop Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.backdropColor },
        { label = "ProgressFrame Border Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.borderColor },
        { label = "ProgressFrame Item Backdrop Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.backdropColor },
        { label = "ProgressFrame Item Border Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.borderColor },
        { label = "ProgressFrame Item Label Text Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.labelTextColor },
        { label = "ProgressFrame Item Title Text Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.titleTextColor },
        { label = "ProgressFrame Item Missing Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.Missing },
        { label = "ProgressFrame Item Normal Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.Normal },
        { label = "ProgressFrame Item Heroic Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.Heroic },
        { label = "ProgressFrame Item Mythic Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.Mythic },
        { label = "ProgressFrame Item Ascended Color", data = SocializeConfig.default.colors.friendInfoFrame.progress.progressItem.Ascended },
    }
    local colorOptionFrames = {}
    local previousParent = { mainFrame = parent }
    for _, colorOption in pairs(colorOptions) do
        previousParent = SocializeSettingsColorOptionFrame:new(colorOption.label, colorOption.data, previousParent.mainFrame)
        table.insert(colorOptionFrames, previousParent)
    end
    return colorOptionFrames
end

function SocializeSettingsColorFrame.initResetButton(parent)
    local resetColorButton = CreateFrame("Button", nil, parent, "GameMenuButtonTemplate")
    resetColorButton:SetSize(80 , 20)
    resetColorButton:SetPoint("TOPRIGHT", -30, -5)
    resetColorButton:SetText("Reset")
    resetColorButton:SetScript("OnClick", function()
        SocializeResetSettingsPopup.popup()
    end)
    return resetColorButton
end

function SocializeSettingsColorFrame:updateComponent()
    for _, colorOption in ipairs(self.colorOptions) do
        colorOption:updateComponent()
    end
end
