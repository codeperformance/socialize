---@class SocializeItemFrame
SocializeItemFrame = {
    mainFrame = nil,
    texture = nil,
    border = nil,
    equipmentFrame = nil,
    slotName = "",
    currentItemLink = "",
    rarityFunctionMap = {
        [2] = "uncommon",
        [3] = "rare",
        [4] = "epic",
        [5] = "legendary",
        [6] = "vanity",
        [7] = "heirloom",
    }
}

function SocializeItemFrame:new(equipmentFrame, slotName, anchorPoint, x, y)
    ---@type SocializeItemFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeItemFrame.__index = self
    instance.equipmentFrame = equipmentFrame
    instance.slotName = slotName
    instance:initMainFrame(anchorPoint, x, y)
    instance:initTexture()
    instance:initBorder()
    instance:activateEventHandlers()
    instance:setDefaultLook()
    return instance
end

function SocializeItemFrame:initMainFrame(anchorPoint, x, y)
    local frame = CreateFrame("Frame", nil, self.equipmentFrame.mainFrame)
    frame:SetSize(32, 32) -- Set the size of the frame
    frame:SetPoint(anchorPoint, x, y)
    frame:EnableMouse(true)
    self.mainFrame = frame
end

function SocializeItemFrame:initTexture()
    local texture = self.mainFrame:CreateTexture(nil,"OVERLAY")
    texture:SetAllPoints()
    texture:SetTexCoord(0.07,0.93,0.07,0.93)
    texture:SetTexture("Interface/PaperDoll/UI-PaperDoll-Slot-Bag")
    self.texture = texture
end

function SocializeItemFrame:initBorder()
    local border = self.mainFrame:CreateTexture(nil, "BORDER")
    --border:SetAllPoints()
    border:SetPoint("CENTER")
    border:SetSize(35, 35)
    self.border = border
end

function SocializeItemFrame:setDefaultLook()
    self.texture:SetTexture("Interface/PaperDoll/UI-PaperDoll-Slot-Bag")
    self.mainFrame:SetScript("OnEnter", nil)
    self.border:SetColorTexture(unpack(SocializeConfig.default.colors.friendInfoFrame.equipment.unavailable))
end

function SocializeItemFrame:setItemLink(itemLink)

    local itemID = string.match(itemLink, "Hitem:(%d+)")
    if self.currentItemLink ~= itemLink then
        self.currentItemLink = itemLink
        TryCacheItem(itemID)
    end

    local _, _, rarity, _, _, _, _, _, _, itemIcon = GetItemInfo(itemLink)
    if SocializeConfig.default.colors.friendInfoFrame.equipment[self.rarityFunctionMap[rarity]] ~= nil then
        self.border:SetColorTexture(unpack(SocializeConfig.default.colors.friendInfoFrame.equipment[self.rarityFunctionMap[rarity]]))
    end
    self.texture:SetTexture(itemIcon)
    self.mainFrame:SetScript("OnEnter", function(self2)
        GameTooltip:SetOwner(self2, "ANCHOR_RIGHT");
        GameTooltip:SetHyperlink(itemLink)
        GameTooltip:Show();
        if IsControlKeyDown() then
            SetCursor("INSPECT_CURSOR")
        end
    end)
end

function SocializeItemFrame:updateComponent()
    local friend = SocializeFriendsFrame.getInstance():getSelectedFriend()
    if friend and friend:hasEquipmentInSlot(self.slotName) then
        self:setItemLink(friend:getEquipmentItemLink(self.slotName))
        return
    end
    self:setDefaultLook()
end

function SocializeItemFrame:activateEventHandlers()
    self.mainFrame:SetScript("OnMouseDown", function(event, button)
        if button == "LeftButton" then
            if IsShiftKeyDown() then
                self:linkItemChat()
            elseif IsControlKeyDown() then
                self:inspectItem()
            end
        end
    end)
    self.mainFrame:SetScript("OnEvent", function(_, event, key)
        if self.mainFrame:IsMouseOver() and key == "LCTRL" or key == "RCTRL" then
            if not IsControlKeyDown() then
                ResetCursor()
            end
        end
    end)
    self.mainFrame:SetScript("OnLeave", function()
        ResetCursor()
        GameTooltip:Hide();
    end)
    self.mainFrame:RegisterEvent("MODIFIER_STATE_CHANGED")
end

function SocializeItemFrame:linkItemChat()
    local friend = SocializeFriendsFrame.getInstance():getSelectedFriend()
    if friend and friend:hasEquipmentInSlot(self.slotName) then
        local itemLink = friend:getEquipmentItemLink(self.slotName)
        for i = 1, NUM_CHAT_WINDOWS do
            local chatFrame = _G["ChatFrame"..i]
            if chatFrame and chatFrame.editBox:IsVisible() then --only add to visible editbox
                chatFrame.editBox:Insert(itemLink)
                break--no need to add it more than once
            end
        end
    end
end

--Function to support Ctrl-click inspecting item
function SocializeItemFrame:inspectItem()
    local friend = SocializeFriendsFrame.getInstance():getSelectedFriend()
    if friend and friend:hasEquipmentInSlot(self.slotName) then
        DressUpItemLink(friend:getEquipmentItemLink(self.slotName))
    end
end
