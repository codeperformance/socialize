---@class SocializeFriendInfoFrame
SocializeFriendInfoFrame = {
    ---@type SocializeFrame
    socializeFrame = {},
    mainFrame = {},
    playerNameTextElement = {},
    playerStatusTextElement = {},
    playerLocationTextElement = {},
    statusPrefix = "Status: ",
    locationPrefix = "Location: ",
    tabs = {},
    friend = {},
    equipmentFrame = {},
    progressFrame = {},
    ---@type SocializeFriendInfoFrame
    instance = nil,
}

function SocializeFriendInfoFrame.getInstance()
    return SocializeFriendInfoFrame.instance
end
---@param socializeFrame SocializeFrame
function SocializeFriendInfoFrame:new(socializeFrame)
    if SocializeFriendInfoFrame.instance then
        return SocializeFriendInfoFrame.instance
    end
    ---@type SocializeFriendInfoFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeFriendInfoFrame.__index = self
    instance.socializeFrame = socializeFrame
    instance:initMainFrame()
    instance:initPlayerNameTextElement()
    instance:initPlayerStatusTextElement()
    instance:initPlayerLocationTextElement()
    instance:initTabFrame()
    instance:activateOnHideEventListener()
    SocializeFriendInfoFrame.instance = instance
    return instance
end

function SocializeFriendInfoFrame:updateComponent()
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendInfoFrame.backdropColor));
    self.mainFrame:SetBackdropBorderColor(unpack(SocializeConfig.default.colors.friendInfoFrame.borderColor));
    self.playerNameTextElement:SetTextColor(unpack(SocializeConfig.default.colors.friendInfoFrame.nameTextColor))
    self.playerNameTextElement:SetText(self.friend.name or "")
    self.playerStatusTextElement:SetTextColor(unpack(SocializeConfig.default.colors.friendInfoFrame.statusTextColor))
    self.playerStatusTextElement:SetText(self.statusPrefix..(self.friend.status or ""))
    self.playerLocationTextElement:SetTextColor(unpack(SocializeConfig.default.colors.friendInfoFrame.locationTextColor))
    self.playerLocationTextElement:SetText(self.locationPrefix..(self.friend.location or ""))
    self.equipmentFrame:updateComponent()
    self.progressFrame:updateComponent()
end


function SocializeFriendInfoFrame:initMainFrame()
    local mainFrame = CreateFrame("Frame", "Player frame", self.socializeFrame.mainFrame)
    mainFrame:SetSize(350, 425)
    mainFrame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 16,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });

    local CloseButton = CreateFrame("Button", nil, mainFrame, "UIPanelCloseButton")
    CloseButton:SetPoint("TOPRIGHT", mainFrame, "TOPRIGHT")
    mainFrame:SetPoint("TOPLEFT", self.socializeFrame.mainFrame, "TOPRIGHT", 15, 0)
    mainFrame:SetFrameLevel(20)
    mainFrame:EnableMouse(true)
    mainFrame:SetMovable(true)
    mainFrame:SetClampedToScreen(true)
    mainFrame:RegisterForDrag("LeftButton")
    mainFrame:SetScript("OnDragStart", mainFrame.StartMoving)
    mainFrame:SetScript("OnDragStop", mainFrame.StopMovingOrSizing)
    mainFrame:Hide()
    self.mainFrame = mainFrame
end

function SocializeFriendInfoFrame:initPlayerNameTextElement()
    local playerNameTextElement = self.mainFrame:CreateFontString(nil, "OVERLAY")
    playerNameTextElement:SetPoint("TOPLEFT", 7, -6) -- This positions the text in the center of the frame
    local _, _, statusFontStyle = playerNameTextElement:GetFont()
    playerNameTextElement:SetFont("Fonts\\FRIZQT__.ttf", 11, statusFontStyle)
    playerNameTextElement:SetText("")
    self.mainFrame.text1 = playerNameTextElement
    self.playerNameTextElement = playerNameTextElement
end

function SocializeFriendInfoFrame:initPlayerStatusTextElement()
    local playerStatusTextElement = self.mainFrame:CreateFontString(nil, "OVERLAY")
    playerStatusTextElement:SetPoint("TOPLEFT", 7, -19) -- This positions the text in the center of the frame
    local _, _, statusFontStyle = playerStatusTextElement:GetFont()
    playerStatusTextElement:SetFont("Fonts\\FRIZQT__.ttf", 11, statusFontStyle)
    playerStatusTextElement:SetText("")
    self.mainFrame.text2 = playerStatusTextElement
    self.playerStatusTextElement = playerStatusTextElement
end

function SocializeFriendInfoFrame:initPlayerLocationTextElement()
    local playerLocationTextElement = self.mainFrame:CreateFontString(nil, "OVERLAY")
    playerLocationTextElement:SetPoint("TOPLEFT", 7, -32) -- This positions the text in the center of the frame
    local _, _, statusFontStyle = playerLocationTextElement:GetFont()
    playerLocationTextElement:SetFont("Fonts\\FRIZQT__.ttf", 11, statusFontStyle)
    playerLocationTextElement:SetText("")
    self.mainFrame.text3 = playerLocationTextElement
    self.playerLocationTextElement = playerLocationTextElement
end

function SocializeFriendInfoFrame:initEquipmentFrame(tab)
    local equipmentFrame = SocializeEquipmentFrame:new(self, tab)
    self.equipmentFrame = equipmentFrame
    return equipmentFrame
end

function SocializeFriendInfoFrame:initProgressFrame(tab)
    local progressFrame = SocializeProgressFrame:new(self, tab)
    self.progressFrame = progressFrame
    return progressFrame
end

function SocializeFriendInfoFrame:initEnchantsTab()

end

function SocializeFriendInfoFrame:initOtherTab()

end

function SocializeFriendInfoFrame:initTabFrame()
    local tabs = {}
    local tabFrames = {}

    local tabNames = {
        "Equipment",
        "Progress",
        "Enchants",
        "Other",
    }
    local anchor = CreateFrame("Frame", nil, self.mainFrame)
    anchor:SetSize(1,1)
    anchor:SetPoint("TOPLEFT", 7, -50)
    -- Create tabs
    for i = 1, 4 do
        tabs[i] = CreateFrame("Button", "MyAddonFrameTab"..i, self.mainFrame, "OptionsFrameTabButtonTemplate")
        tabs[i]:SetText(tabNames[i])

        --tabs[i]:SetSize(width,24);
        tabs[i]:SetPoint("TOPLEFT", anchor, "TOPRIGHT", -8, 0)
        anchor = tabs[i]

        if tabNames[i] == "Equipment" then
            tabFrames[i] = self:initEquipmentFrame(tabs[1]).mainFrame--tab 1 for anchor
        elseif tabNames[i] == "Progress" then
            tabFrames[i] = self:initProgressFrame(tabs[1]).mainFrame--tab 1 for anchor
        else
            -- Create corresponding tab frame
            tabFrames[i] = CreateFrame("Frame", nil, self.mainFrame)
            tabFrames[i]:SetPoint("TOPLEFT", tabs[1], "BOTTOMLEFT")
            tabFrames[i]:SetPoint("BOTTOMRIGHT", self.mainFrame, "BOTTOMRIGHT")
            --tabFrames[i]:SetAllPoints(true)
            tabFrames[i]:SetBackdrop({
                bgFile = "Interface/Tooltips/UI-Tooltip-Background",
                edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
                tile = true, tileSize = 16, edgeSize = 16,
                insets = { left = 0, right = 0, top = 0, bottom = 0 }
            });
            tabFrames[i]:SetBackdropColor(0, 0, 0, 0)
            tabFrames[i]:SetBackdropBorderColor(1, 1, 1, 1)

            -- Add some content for demo
            local text = tabFrames[i]:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
            text:SetText("Landing page for "..tabNames[i].." WIP")
            text:SetPoint("CENTER")
        end


        -- OnClick event
        tabs[i]:SetScript("OnClick", function()
            for j = 1, #tabs do
                local tabText = _G[tabs[j]:GetName().."Text"] -- get the FontString
                if j == i then
                    --tabs[j]:SetBackdropColor(0.2, 0.2, 0.2, 0.7)
                    tabText:SetTextColor(1,0.5,0);
                    tabFrames[j]:Show();
                else
                    --tabs[j]:SetBackdropColor(0,0,0, 0.7);
                    tabText:SetTextColor(1, 0.8, 0.0);
                    tabFrames[j]:Hide();
                end
            end
        end);
    end

    -- Select first tab by default
    tabs[1]:Click();
    self.tabs = tabs
end

function SocializeFriendInfoFrame:setFriend(friend)
    self.friend = friend
end

function SocializeFriendInfoFrame:hasFriend(friend)
    return self.friend == friend
end

function SocializeFriendInfoFrame:isVisible()
    return self.mainFrame:IsVisible()
end

function SocializeFriendInfoFrame:show()
    self.mainFrame:Show()
end

function SocializeFriendInfoFrame:hide()
    self.mainFrame:Hide()
end

function SocializeFriendInfoFrame:activateOnHideEventListener()
    self.mainFrame:SetScript("OnHide", function()
        --SocializeFriendsFrame.getInstance():updateComponent()
    end)

end