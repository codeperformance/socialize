---@class SocializeFriendFrame
SocializeFriendFrame = {
    ---@type Friend
    friend = {},
    mainFrame = {},
    icon = {},
    nameString = {},
    locationString = {},
    statusString = {},
    menu = {},
    dropdown = {},
    isPartOfSearchResult = false, --required to prevent to sort on hidden status, as that would make UI glitch every time you reopen panel
    friendsFrame = {},
    isDeleted = false,
}

function SocializeFriendFrame:new(friendsFrame, friend, parentLayer, anchorTo)
    ---@type SocializeFriendFrame
    local instance = {}
    setmetatable(instance, self)
    SocializeFriendFrame.__index = self
    instance.friendsFrame = friendsFrame
    instance.friend = friend
    instance:initializeMainFrame(parentLayer, anchorTo)
    instance:initializeIcon()
    instance:initializeNameString()
    instance:initializeLocationString()
    instance:initializeStatusString()
    instance:initializeMenuItems()
    instance:initializeDropdown()
    instance:activateMouseDownListener()
    instance.mainFrame:Show()
    return instance
end

function SocializeFriendFrame:updateComponent()
    self.locationString:SetTextColor(unpack(SocializeConfig.default.colors.friendFrame.locationTextColor))
    self.nameString:SetTextColor(unpack(SocializeConfig.default.colors.friendFrame.nameTextColor))
    if self:isOnline() then
        self:setOnline()
        self:setBackdropColorOnline()
    else
        self:setOffline()
        self:setBackdropColorOffline()
    end
    if not self.friend.location then
        self.friend.location = ""
    end
    self:setLocation(self.friend.location)
    if not self.friend.status then
        self.friend.status = ""
    end
    if self.friend.status == "RELOADING" then
        self:setStatusColorWarning()
    else
        self:setStatusColorNormal()
    end

    if self.friend.ignored then
        self:setBackdropColorIgnored()
    end

    local friendInfoFrame = self.friendsFrame.socializeFrame.friendInfoFrame
    if friendInfoFrame:isVisible() and self.friend == friendInfoFrame.friend then
        self:setHighlightedBackgroundColor()
    end

    if SocializeSession.isSelf(self.friend.name) then
        self.mainFrame:SetBackdropBorderColor(unpack(SocializeConfig.default.colors.friendFrame.selfBorderColor));
    else
        self.mainFrame:SetBackdropBorderColor(unpack(SocializeConfig.default.colors.friendFrame.borderColor));
    end

    self:setStatus(self.friend.status)
    self:refreshName()
end

function SocializeFriendFrame:isVisible()
    return self.mainFrame:IsVisible()
end

function SocializeFriendFrame:show(friend)
    self.friend = friend
    self.mainFrame:Show()
end

function SocializeFriendFrame:remove()
    self.isDeleted = true
    self.mainFrame:Hide()
end

function SocializeFriendFrame:anchorTo(anchorTo)
    self.mainFrame:SetPoint("TOPLEFT", anchorTo, "BOTTOMLEFT")
end

function SocializeFriendFrame:setOnline()
    self.icon:SetTexture("Interface/FriendsFrame/StatusIcon-Online")
end

function SocializeFriendFrame:setOffline()
    self.icon:SetTexture("Interface/FriendsFrame/StatusIcon-Offline")
end

function SocializeFriendFrame:setHighlightedBackgroundColor()
    return self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendFrame.backdropColorHighlighted))
end

function SocializeFriendFrame:isOnline()
    return self.friend.online
end

function SocializeFriendFrame:setLocation(location)
    self.locationString:SetText(location)
end

function SocializeFriendFrame:setStatusColorWarning()
    self.statusString:SetTextColor(unpack(SocializeConfig.default.colors.friendFrame.statusTextColorReloading))
end

function SocializeFriendFrame:setStatusColorNormal()
    self.statusString:SetTextColor(unpack(SocializeConfig.default.colors.friendFrame.statusTextColorNormal))
end

function SocializeFriendFrame:setStatus(status)
    self.statusString:SetText(status)
end

function SocializeFriendFrame:refreshName()
    self.nameString:SetText(self:resolveFriendName())
end

function SocializeFriendFrame:setBackdropColorOnline()
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendFrame.backdropColorOnline))
end

function SocializeFriendFrame:setBackdropColorOffline()
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendFrame.backdropColorOffline))
end

function SocializeFriendFrame:setBackdropColorIgnored()
    self.mainFrame:SetBackdropColor(unpack(SocializeConfig.default.colors.friendFrame.backdropColorIgnored))
end

function SocializeFriendFrame:initializeMainFrame(parentLayer, anchorTo)
    local mainFrame = CreateFrame("Frame", self.friend.name, parentLayer)
    mainFrame:SetSize(325, 35)  -- adjust size as needed
    mainFrame:SetPoint("TOPLEFT", anchorTo, "BOTTOMLEFT")  -- change this to position where you want
    mainFrame:SetBackdrop({
        bgFile = "Interface/Buttons/WHITE8X8",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true,
        tileSize = 16,
        edgeSize = 12,
        insets = { left = 4, right = 4, top = 4, bottom = 4 }
    });
    mainFrame:EnableMouse(true)
    self.mainFrame = mainFrame
end

function SocializeFriendFrame:initializeIcon()
    local icon = self.mainFrame:CreateTexture(self.friend.name.."icon", "ARTWORK", self.mainFrame)
    icon:SetPoint("LEFT", self.mainFrame, "LEFT", 5, 0)
    icon:SetSize(21,21)
    icon:SetTexture("Interface/FriendsFrame/StatusIcon-Offline")  -- set the path to your icon

    if self.friend.online == true then
        icon:SetTexture("Interface/FriendsFrame/StatusIcon-Online")
    end
    self.mainFrame.texture = icon
    self.icon = icon
end

function SocializeFriendFrame:resolveFriendName()
    local name = nil
    if self.friend.hasManualID then
        name = self.friend.ID
        if self.friend.ID ~= self.friend.name then
            name = name.."/"..self.friend.name
        end
    end
    if not name then
        name = self.friend.name
    end
    return name
end

function SocializeFriendFrame:initializeNameString()
    local nameString = self.mainFrame:CreateFontString(self.friend.name.."name", "OVERLAY")
    nameString:SetPoint("TOPLEFT", 25, -6) -- This positions the text in the center of the frame
    local name_font, _, name_font_style = nameString:GetFont()
    nameString:SetFont("Fonts\\FRIZQT__.ttf", 11, name_font_style)
    nameString:SetText(self:resolveFriendName())
    self.mainFrame.text1 = nameString
    self.nameString = nameString
end

function SocializeFriendFrame:initializeLocationString()
    local locationString = self.mainFrame:CreateFontString(self.friend.name.."location", "OVERLAY")
    locationString:SetPoint("BOTTOMLEFT", 25, 6) -- This positions the text in the center of the frame
    local _, _, location_font_style = locationString:GetFont()
    locationString:SetFont("Fonts\\FRIZQT__.ttf", 9, location_font_style)
    locationString:SetText("")
    self.mainFrame.text2 = locationString
    self.locationString = locationString
end

function SocializeFriendFrame:initializeStatusString()
    local statusString = self.mainFrame:CreateFontString(self.friend.name.."status", "OVERLAY")
    statusString:SetPoint("TOPRIGHT", -7, -6) -- This positions the text in the center of the frame
    local _, _, statusFontStyle = statusString:GetFont()
    statusString:SetFont("Fonts\\FRIZQT__.ttf", 11, statusFontStyle)
    statusString:SetText("")
    self.mainFrame.text3 = statusString
    self.statusString = statusString
end

function SocializeFriendFrame:shareEquipment()
    SocializeShareEquipmentPopup.popup(self.friend.name);
end

function SocializeFriendFrame:shareProgress()
    SocializeShareProgressPopup.popup(self.friend.name);
end

function SocializeFriendFrame:pst()
    ChatFrame_SendTell(self.friend.name)
end

function SocializeFriendFrame:partyInvite()
    if IsInGroup("player") then
        SuggestInvite(self.friend.name)
    else
        InviteUnit(self.friend.name)
    end
end

function SocializeFriendFrame:sendReRequest()
    SocializeCommunicationInternal.sendFriendRequest(self.friend.name)
end

function SocializeFriendFrame:toggleIgnored()
    SocializeFriendIgnorePopup.popup(self.friend)
end

function SocializeFriendFrame:removeFriend()
    SocializeFriendRemovePopup.popup(self.friend.name)
    SocializeFriendWrapper.removeReference(self.friend.name)
end

function SocializeFriendFrame:initializeMenuItems()
    local isSelf = SocializeSession.isSelf(self.friend.name)
    local isMe = self.friend.ID == "Me"
    local menu = {}
    table.insert(menu, { text = "Options for "..self.friend.name, isTitle = true})
    if not isMe then
        table.insert(menu, { text = "Set nickname", func = function() SocializeSetIDPopup.popup(self.friend); CloseDropDownMenus() end })
    end
    table.insert(menu, {
        text = "Share", hasArrow = true,
        menuList = {
            { text = "Equipment", func = function() self:shareEquipment(); CloseDropDownMenus() end },
            { text = "Progress", func = function() self:shareProgress(); CloseDropDownMenus() end },
          },
        })
    local friendAlts = SocializeStorage.findFriendsByID(self.friend.ID)
    local friendAltContextMenus = {}
    for altName, friend in pairs(friendAlts) do
        if self.friend.name ~= friend.name then
            table.insert(friendAltContextMenus, { text = friend.name, func = function() SocializeFriendFrame.selectFriend(friend) CloseDropDownMenus() end })
        end
    end
    if #friendAltContextMenus > 0 then
        table.insert(menu, {
                text = "Alts", hasArrow = true,
                menuList = friendAltContextMenus
            })
    end
    table.insert(menu,{ text = "PST", func = function() self:pst(); CloseDropDownMenus() end })
    if not isMe then
        table.insert(menu,{ text = "Invite to party", func = function() self:partyInvite(); CloseDropDownMenus() end })
        table.insert(menu,{ text = "Friend re-request", func = function() self:sendReRequest(); CloseDropDownMenus() end })
        table.insert(menu,{ text = self:getContextMenuIgnoreOptionText(self.friend), func = function() self:toggleIgnored(); CloseDropDownMenus() end })
    end
    if not isSelf then
        table.insert(menu,{ text = "Remove friend", func = function() self:removeFriend(); CloseDropDownMenus() end })
    end
    table.insert(menu,{ text = "Close", func = function() CloseDropDownMenus() end })
    self.menu = menu
end

function SocializeFriendFrame:initializeDropdown()
    self.dropdown = CreateFrame("Frame", self.friend.name.."MyAddonContextMenu", UIParent, "UIDropDownMenuTemplate")
end

function SocializeFriendFrame:activateMouseDownListener()
    local lastClickTime
    local manualCheckAfterSeconds = 3
    self.mainFrame:SetScript("OnMouseDown", function(_, button)
        -- < Manual Ping >
        if not self.friend.ignored then
            if not SocializeSession.isSelf(self.friend.name) then
                local timeNow = time()
                if not lastClickTime or lastClickTime < timeNow - manualCheckAfterSeconds then
                    lastClickTime = timeNow
                    SocializeCommunication.pingFriend(self.friend, 3, 6)
                end
            end
        end
        -- </ Manual Ping >
        if button == "RightButton" then
            self:initializeMenuItems()
            EasyMenu(self.menu, self.dropdown, "cursor", 0 , 0, "MENU");
        end
        if button == "LeftButton" then
            self.selectFriend(self.friend)
        end
    end)
end

function SocializeFriendFrame.selectFriend(friend)
    local friendsFrame =  SocializeFriendsFrame.getInstance()
    local friendInfoFrame = friendsFrame.socializeFrame.friendInfoFrame
    if friendInfoFrame:isVisible() then
        if friendInfoFrame:hasFriend(friend) then
            friendInfoFrame:hide()
        else
            friendInfoFrame:setFriend(friend)
            friendInfoFrame:show()
            SocializeFriendsFrame.getInstance():setSelectedFriend(friend)
        end
    else
        friendInfoFrame:setFriend(friend)
        friendInfoFrame:show()
        SocializeFriendsFrame.getInstance():setSelectedFriend(friend)

    end
    --friendsFrame:updateComponent()
    --friendInfoFrame:updateComponent()
end

function SocializeFriendFrame:getContextMenuIgnoreOptionText(friend)
    if friend.ignored then
        return "Un-ignore friend"
    else
        return "Ignore friend"
    end
end