SocializeString = {}

function SocializeString.startsWith(str, prefix)
    return str:sub(1, #prefix) == prefix
end

function SocializeString.split(inputString, separator)
    if not separator then
        separator = "%s"
    end
    local t = {}
    for str in string.gmatch(inputString, "([^".. separator .."]+)") do
        table.insert(t, str)
    end
    return t
end

function SocializeString.randomAlphabeticString(length)
    local str = ""
    for i = 1, length do
        local ascii = 0
        if math.random(2) == 1 then
            ascii = math.random(65, 90) -- Uppercase letters
        else
            ascii = math.random(97, 122) -- Lowercase letters
        end
        str = str .. string.char(ascii)
    end
    return str
end

