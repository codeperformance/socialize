SocializeCommunicationInternal = {
    debug = false,
}

function SocializeCommunicationInternal.debugMessages(event, friendName)
    if SocializeCommunicationInternal.debug then
        print(event, "", "WHISPER", friendName)
    end
end

function SocializeCommunicationInternal.sendFriendRequest(friendName)
    SocializeCommunicationInternal.debugMessages(SocializeCommunicationEventNames.friendRequestEvent, friendName)
    SendAddonMessage(SocializeCommunicationEventNames.friendRequestEvent, "", "WHISPER", friendName)
end

function SocializeCommunicationInternal.sendIGoOffline(friendName)
    if Socialize.showOnline then
        SocializeCommunicationInternal.debugMessages(SocializeCommunicationEventNames.iGoOfflineEvent, friendName)
        SendAddonMessage(SocializeCommunicationEventNames.iGoOfflineEvent, "", "WHISPER", friendName)
    end
end

function SocializeCommunicationInternal.sendAreYouOnline(friendName)
    local friend = SocializeStorage.getFriendWrapper(friendName)
    if Socialize.showOnline then
        SocializeCommunicationInternal.debugMessages(SocializeCommunicationEventNames.areYouOnlineEvent, friendName)
        SendAddonMessage(SocializeCommunicationEventNames.areYouOnlineEvent, friend:createIdentityPackage(), "WHISPER", friendName)
    end
end

function SocializeCommunicationInternal.sendIAmOnline(friendName)
    if Socialize.showOnline then
        SocializeCommunicationInternal.debugMessages(SocializeCommunicationEventNames.iAmOnlineEvent, friendName)
        SendAddonMessage(SocializeCommunicationEventNames.iAmOnlineEvent, SocializeStorage.createDataPackage(), "WHISPER", friendName)
    end
end

function SocializeCommunicationInternal.sendIAmReloading(friendName)
    if Socialize.showOnline then
        SocializeCommunicationInternal.debugMessages(SocializeCommunicationEventNames.iAmReloadingEvent, friendName)
        SendAddonMessage(SocializeCommunicationEventNames.iAmReloadingEvent, "", "WHISPER", friendName)
    end
end

function SocializeCommunicationInternal.sendEquipment(friendName, slotName)
    SocializeCommunicationInternal.debugMessages(SocializeCommunicationEventNames.shareEquipmentEvent, friendName)
    SendAddonMessage(SocializeCommunicationEventNames.shareEquipmentEvent, slotName.."\1"..(GetInventoryItemLink("player", SocializeEquipmentSlotMapping[slotName]) or ""), "WHISPER", friendName)
end

function SocializeCommunicationInternal.sendProgress(friendName, progressType, progressKey, progressValue)
    SocializeCommunicationInternal.debugMessages(SocializeCommunicationEventNames.shareProgressEvent, friendName)
    SendAddonMessage(SocializeCommunicationEventNames.shareProgressEvent, progressType .."\1"..progressKey.."\1"..progressValue , "WHISPER", friendName)
end

function SocializeCommunicationInternal.sendAllEquipment(friendName)
    SocializePrint.systemMessage("Sharing equipment with "..friendName)
    for slotName, slotID in pairs(SocializeEquipmentSlotMapping) do
        SocializeCommunicationInternal.sendEquipment(friendName, slotName)
    end
end

function SocializeCommunicationInternal.sendAllProgress(friendName)

    SocializePrint.systemMessage("Sharing progress with "..friendName)
    for _, progressType in ipairs(SocializeProgress.releasedProgressTypes) do
        for progressKey, difficulty in pairs(SocializeProgress.getProgress(progressType)) do
            SocializeCommunicationInternal.sendProgress(friendName, progressType, progressKey, difficulty)
        end
    end
end
